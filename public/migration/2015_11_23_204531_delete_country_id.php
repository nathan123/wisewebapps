<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCountryId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropForeign(['countries_id']);
			$table->dropColumn('countries_id');

		});
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->integer('countries_id')->unsigned();
			$table->foreign('countries_id')->references('id')->on('countries')
                  ->onUpdate('cascade')->onDelete('cascade');
		});
	}

}
