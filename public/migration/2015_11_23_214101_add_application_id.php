<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApplicationId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('customers_documents', function(Blueprint $table)
		{
			$table->integer('applications_id')->unsigned();
			$table->foreign('applications_id')->references('id')->on('applications')
                  ->onUpdate('cascade')->onDelete('cascade');
		});


		Schema::table('customers', function(Blueprint $table)
		{
			$table->integer('applications_id')->unsigned();
			$table->foreign('applications_id')->references('id')->on('applications')
                  ->onUpdate('cascade')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('customers_documents', function(Blueprint $table)
		{
			$table->dropColumn('applications_id');
		});

		Schema::table('customers', function(Blueprint $table)
		{
			$table->dropColumn('applications_id');
		});
	}

}
