<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApplication extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropForeign(['countries_id']);
			$table->dropColumn('countries_id');

		});

		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		Schema::table('customers', function(Blueprint $table)
		{
			$table->integer('applications_id')->unsigned();
			$table->foreign('applications_id')->references('id')->on('applications')
                  ->onUpdate('cascade')->onDelete('cascade');
		});

		Schema::create('customers_documents', function(Blueprint $table)
		{
			$table->integer('customers_id')->unsigned();	
			$table->foreign('customers_id')->references('id')->on('customers')
                  ->onUpdate('cascade')->onDelete('cascade');


            $table->integer('documents_id')->unsigned();	
			$table->foreign('documents_id')->references('id')->on('documents')
                  ->onUpdate('cascade')->onDelete('cascade');

            $table->integer('applications_id')->unsigned();
			$table->foreign('applications_id')->references('id')->on('applications')
                  ->onUpdate('cascade')->onDelete('cascade');

            $table->boolean('is_available')->default(false);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('customers', function(Blueprint $table)
		{
			
            $table->dropColumn('applications_id');


		});

		Schema::drop('customers_documents');

		Schema::table('customers_documents', function(Blueprint $table)
		{
			$table->dropColumn('applications_id');
		});

	}

}
