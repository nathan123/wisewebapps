<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers_documents', function(Blueprint $table)
		{
			$table->integer('customers_id')->unsigned();	
			$table->foreign('customers_id')->references('id')->on('customers')
                  ->onUpdate('cascade')->onDelete('cascade');


            $table->integer('documents_id')->unsigned();	
			$table->foreign('documents_id')->references('id')->on('documents')
                  ->onUpdate('cascade')->onDelete('cascade');

            $table->boolean('is_available')->default(false);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers_documents');

	}

}
