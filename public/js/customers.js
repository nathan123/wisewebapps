	/**
 * Main Javascript For Juab Title
 * all javascript functionality
 * extend Report
 * Author : Angelo
 * v 0.0.1.0
 **/
;(function($) {
    $.customers = {
        error :  'This field is required.',
        number : 'Invalid Number',
        /**
         * function : init
         */
        init : function() {
      
        	$.ajaxSetup({
			  headers: {
			    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			  }
			});

        	$('#myModal').on('hidden.bs.modal', function () {
                location.reload();
            });

            $('span.alert-success').delay(2000).fadeOut('slow', function(){
               // location.reload(true);
            });

        	 /**
	         * function : To count activity Logs every 2 secs
	         */

        	setInterval( this._countrefresh, 2000 );

        	this.addcustomer();
        	this._addeducationfield();
        	this._addworkhistory();
        	this._addlanguage();
        	this.addResume();
        	this.addfamily();
        	this.addeducation();
        	this.addworkhistory();
        	this.addlanguage();
        	this.addcomment();
        	this.addspouse();
        	this.addspouseeducation();
        	this.addspouseworkhistory(); 
        	this.addspouselanguage();
        	this.changeresume();
        	this._approvecustomer();
        	this._disapprovecustomer();
        	this._searchname();
        	this._numtype();
        	this._countrefresh();
        	this.editlanguage();
        	this._holdcustomer();
        	this._unholdcustomer();
        	
        	if( this._urlParams('success_customer') != null ) {
				$("form#addcustomer").before('<div class="alert alert-success">Successfully Added Customer Information!</div>');
			}

			if( this._urlParams('customersfamily') != null ) {
				$("form#addfamily").before('<div class="alert alert-success">Successfully Added Family Composition</div>');
			}

			if( this._urlParams('customerseducation') != null ) {
				$("form#addeducation").before('<div class="alert alert-success">Successfully Added Education </div>');
			}

			if( this._urlParams('customersworkhistory') != null ) {
				$("form#addwork").before('<div class="alert alert-success">Successfully Added Work History </div>');
			}

			if( this._urlParams('customerslanguage') != null ) {
				$("form#addlanguage").before('<div class="alert alert-success">Successfully Added Language </div>');
			}
			
			if( this._urlParams('success_customer_spouse') != null ) {
				$("form#addspouse").before('<div class="alert alert-success">Successfully Added Spouse </div>');
			}

			if( this._urlParams('customersspouseeducation') != null ) {
				$("form#addspouseeducation").before('<div class="alert alert-success">Successfully Added Spouse Education</div>');
			}

			if( this._urlParams('customersspousework') != null ) {
				$("form#addspousework").before('<div class="alert alert-success">Successfully Added Spouse Work History </div>');
			}

			if( this._urlParams('customersspouselanguage') != null ) {
				$("form#addspouselanguage").before('<div class="alert alert-success">Successfully Added Spouse Language </div>');
			}

			if( this._urlParams('comment') != null ) {
				$("form#customerinfo").before("<span class='alert alert-success' style='display: block'> Successfully Added Comment.</span>");
			}

			if( this._urlParams('approve') != null ) {
				$("form#customerinfo").before("<span class='alert alert-success' style='display: block'> Successfully Aprroved The Customer.</span>");
			}

			if( this._urlParams('disapprove') != null ) {
				$("form#customerinfo").before("<span class='alert alert-danger' style='display: block'> Successfully Disapprove The Customer.</span>");
			}



        },


        addcustomer : function( ) {

        	var t 	 	= this;

        	$('#addcustomermodal').on('hidden.bs.modal', function () {
                location.reload();
            });
            
        	$("form#addcustomer").submit(function(){

        		var isValid = true;      		

				$(this).find("span.error").remove();				
				//validate email
				if ( !t._validateEmail( $("input[name='email']").val() ) ) {
					isValid = false;
				}

				if( $("input[name='gender']:checked") == false ) {
					$(".gender_error").html('<span class="error">'+ t.error +'</span>');
					isValid = false;
				}
				
				if( t._validateForm( isValid ) ) {

					$.post(	'store',
							{

								'_token'         : $("input[name='_token']").val(),
								'applications_id' : $("select[name='applications_id']").val(),
								'source' 		 : $("select[name='source']").val(),
								'firstname'      : $("input[name='firstname']").val(),
								'lastname'    	 : $("input[name='lastname']").val(),
								'middle_name'    : $("input[name='middle_name']").val(),
								'email'   		 : $("input[name='email']").val(),
								'date_of_birth'  : $("input[name='date_of_birth']").val(),
								'gender' 		 : $("input[name='gender']:checked").val(),
								'contact_number' : $("input[name='contact_number']").val(),
								'profession' 	 : $("input[name='profession']").val(),
								'user_id' 	     : $("select[name='user_id']").val(),
								'city'  	               : $("input[name='city']").val(),
								'province'                 : $("input[name='province']").val(),
								'country'  	               : $("select[name='country']").val(),
								'file'  	               : $("input[name='file_id']").val(),
								'secondary_email'  	       : $("input[name='secondary_email']").val(),
								'other_email'  	     	   : $("input[name='other_email']").val(),
								'secondary_contact_number' : $("input[name='secondary_contact_number']").val(),
								'other_contact_number'     : $("input[name='other_contact_number']").val(),								
								'meta_description_first'   : $("textarea[name='meta_description_first']").val(),
								'meta_description_second'  : $("textarea[name='meta_description_second']").val()

					}).done( function( data ){


						if( data == 'email' ) {
							$(".exist-error").remove();
							$("form#addcustomer").before('<div class="alert alert-danger exist-error"> Email Already Exist!</div>');
						} 

						else if( data == 'name' ){
							$(".exist-error").remove();
							$("form#addcustomer").before('<div class="alert alert-danger exist-error"> Name Already Exist!</div>');
						}

						else {

							// if( t._urlParams('success_customer') == null ) {	
							// 	window.location = location.href+'/?success_customer=true';
							// }

							// else {
							// 	location.reload( true );
							// }
							// $("#addcustomermodal").modal('show');
							$("form#addcustomer").find("input[type='text'], input[type='date'], input[type='file'], input[type='email'], textarea, select").val("");
							$("form#addcustomer").find("input[name='gender']").prop('checked', false);
							$("form#addcustomer").before('<div class="alert alert-success" id="customersaddsuccess">Successfully Added Customer Information!</div>');
							$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

							$('div#customersaddsuccess').delay(2000).fadeOut('slow', function(){
				                location.reload(true);
				            });

						}

						console.log( data );

					}, "json");


					
				}

				return false;
        	});

        },

        addResume : function() {

        	var t = this;

        	$("form#addcustomer").find("#file").change(function(){

        		var file_data = $(this).prop('files')[0];
				var form_data = new FormData();
				var token     = $(this).parents("form").find("input[name='_token']").val();
				var fileExtension = ['doc', 'pdf', 'docx'];
				var isValid   = true;

				 if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		            alert("Only formats are allowed : "+fileExtension.join(', '));
		            $(this).val("");
		            isValid = false;
		        }

		        if( isValid ){
					form_data.append( 'file', file_data );
					form_data.append( '_token', token );
					
					if( $("input[name='file_id']").val() != null )
						t.deleteresume( $("input[name='file_id']").val(), token );

					$("input[name='file_id']").remove();

					$.ajax({

						method : "POST",
						url  : "uploadresume",
						dataType: "json",
					    data: form_data,
					    contentType: false,
					    processData: false
						    
					}).done(function( data ){

						$("#file").after("<input type='hidden' name='file_id' value='"+data.id+"' />");

					});
				}
        	});


        },

        changeresume : function() {
        	var t = this;
        	$("form#editcustomers").find("#changefile").change(function(){

        		var file_data = $(this).prop('files')[0];
				var form_data = new FormData();
				var token     = $(this).parents("form").find("input[name='_token']").val();
				var url 	  = $(this).data('url');
				var fileExtension = ['doc', 'pdf', 'docx'];
				var isValid   = true;

				 if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		            alert("Only formats are allowed : "+fileExtension.join(', '));
		            $(this).val("");
		            isValid = false;
		        }

		        if( isValid ){
					form_data.append( 'file', file_data );
					form_data.append( '_token', token );
					
					if( $("input[name='file_id']").val() != null ) {

						$.post( url + '/admin/customers/deleteresume/'+ $("input[name='file_id']").val() , {
							'_token' : token,
							success : function( data ) {

								console.log( data );
							}
						});
					}
					
					$(".file_name").remove();
					$("input[name='file_id']").remove();

					$.ajax({

						method : "POST",
						url  :  url + "/admin/customers/uploadresume",
						dataType: "json",
					    data: form_data,
					    contentType: false,
					    processData: false
						    
					}).done(function( data ){

						$("#changefile").prev().before("<div class='form-group'><div class='file_name'> <a href='/uploads/"+data.name+"' class='btn btn-warning' download>Download CV</a> </div></div> ");
						$("#changefile").after("<input type='hidden' name='file_id' value='"+data.id+"' />");
						console.log( data );

					});
				}
        	});

        },

        addfamily : function() {

        	var t = this;
        	$('#addfamilymodal').on('hidden.bs.modal', function () {
                location.reload();
            });

             $("input[name='number_of_children']").keypress(function(key){
                if(key.charCode < 48 || key.charCode > 57) return false;
            });

        	$("form#addfamily").submit(function(){

        		var isValid = true;       		
				$(this).find("span.error").remove();
				
					if( t._validateForm( isValid ) ) {

						$.post( $(this).attr('action') + '/' + $("input[name='customers_id']").val(), {

							'_token'   : $("input[name='_token']").val(),
							'status'   : $("select[name='status']").val(),
							'number_of_children'  : $("input[name='number_of_children']").val(),		

						}).done(function( data ){

							if( data ) {	
								$("form#addfamily").find("input[type='text'], select").val("");
								$("form#addfamily").before('<div class="alert alert-success" id="addfamilymessage">Successfully Added Family Composition</div>');
								$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

								$('div#addfamilymessage').delay(2000).fadeOut('slow', function(){
					                location.reload(true);
					            });
							}

						
							console.log( data );

						}, "json");

					}

           		return false;
        	});

        },

        addeducation: function() {

        	var t = this;

        	 $('#addeducationmodal').on('hidden.bs.modal', function () {
                location.reload();
            });

        	$("form#addeducation").submit(function(){

        		//console.log( $(this).serialize() );
        		//return false;
        		console.log( $(this).serialize() );
        		var isValid  = true;       		
        		$(this).find("span.error").remove();
				var educ_len = $("input[name='count']").length;

				for ( var i = 1; i <= educ_len; i++ ) {

					var startDate = new Date( $("input[name='from"+i+"']").val() );
					var endDate   = new Date( $("input[name='to"+i+"']").val() );
					
					if ( startDate > endDate ) {

						$("input[name='from"+i+"']").after('<span class="error">Start Date Must Be Lesser than End Date</span>');
						isValid = false;
					}

				}

				if( t._validateForm( isValid ) ) {
					$.post( $(this).attr('action'), {

						'_token' : $("input[name='_token']").val(),
						count 	 : educ_len,
						data 	 : $( this ).serialize()
						
					}).done(function( data ){

							if( data ) {	
								$("form#addeducation").find("input[type='text'], input[type='date'], select").val("");
								$("form#addeducation").before('<div class="alert alert-success" id="educationmessage">Successfully Added Education </div>');
								$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

								$('div#educationmessage').delay(2000).fadeOut('slow', function(){
					                location.reload(true);
					            });
							}

							console.log( data );

					}, "json");
				}
				return false;

        	});
        },

        addworkhistory : function() {

        	var t = this;

        	$("form#addwork").submit(function(){

        		console.log( $(this).serialize() );
        		var isValid = true; 
        		$(this).find("span.error").remove();
        		var work_len = $("input[name='count']").length;


        		for ( var i = 1; i <= work_len; i++ ) {

					var startDate = new Date( $("input[name='from"+i+"']").val() );
					var endDate   = new Date( $("input[name='to"+i+"']").val() );
					
					if ( startDate > endDate ) {

						$("input[name='from"+i+"']").after('<span class="error">Start Date Must Be Lesser than End Date</span>');
						isValid = false;
					}

				}

				if( t._validateForm( isValid ) ) {
					$.post( $(this).attr('action'), {

						'_token' : $("input[name='_token']").val(),
						count 	 : work_len,
						data 	 : $( this ).serialize()
						
					}).done(function( data ){

						if( data ) {	
							$("form#addwork").find("input[type='text'], input[type='date']").val("");
							$("form#addwork").before('<div class="alert alert-success" id="workmessage">Successfully Added Customer Work History.</div>');
							$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

							$('div#workmessage').delay(2000).fadeOut('slow', function(){
				                location.reload(true);
				            });

						}
						console.log( data );


					}, "json");
				}
				return false;

        	});

        },

        addlanguage : function() {

        	var t = this;

        	$("form#addlanguage").submit(function(){

        		console.log( $(this).serialize() );
        		var isValid = true;   

				$(this).find("span.error").remove();
				$(this).find("div.alert-success").remove();

				if( t._validateForm( isValid ) ) {

					$.post( $(this).attr('action'), {

						'_token' : $("input[name='_token']").val(),
						count 	 : $("input[name='count']").length,
						data 	 : $( this ).serialize()
						
					}).done(function( data ){

						if( data ) {	

							$("form#addlanguage").find("input[type='text']").val("");
							$("form#addlanguage").before('<div class="alert alert-success" id="languagemessage">Successfully Added Customer Language.</div>');
							$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

							$('div#languagemessage').delay(2000).fadeOut('slow', function(){
				                location.reload(true);
				            });

						}
						console.log( data );


					}, "json");
				}
				return false;

        	});
        },

        _addeducationfield : function() {

        		var i = 2;

        		$(".addeducationfield").click(function() {

					// $("table tbody tr:first").clone().find(".form-control").each(function() {
					//     $(this).attr({
					//       'name': function(_, name) { return name + i },
					//       'value': ''               
					//  	});
					//   	}).end().appendTo("table");
					// i++;

					$("table tbody tr.add_education_info").last().after(
						'<tr class="add_education_info">\
						<input type="hidden" name="count" />\
						    <td>\
						        <div class="form-group">\
						            <select class="form-control" name="status'+i+'">\
						                <option value="graduate">Graduate</option>\
						                <option value="undergraduate">UnderGraduate</option>\
						                <option value="ongoing">On Going</option>\
						                <option value="postgrad">Post Graduate</option>\
						            </select>\
						        </div>\
						    </td>\
						    <td>\
						        <div class="form-group">\
						            <input class="form-control required" name="name_of_school'+i+'" type="text">\
						        </div>\
						    </td>\
						    <td>\
						        <div class="form-group">\
						            <input type="date" name="from'+i+'" class="form-control required">\
						        </div>\
						    </td>\
						    <td>\
						        <div class="form-group">\
						            <input type="date" name="to'+i+'" class="form-control required">\
						        </div>\
						    </td>\
						    <td>\
						        <div class="form-group">\
						            <input class="form-control required" name="qualification'+i+'" type="text">\
						        </div>\
						    </td>\
						</tr>'
					);
					i++;
				});

        },

        _addworkhistory : function() {

        	var i = 2;

        		$(".addworkhistory").click(function() {
        			$("table tbody tr.add_work_history").last().after(
	        			'<tr class="add_work_history">\
						  <input type="hidden" name="count">\
						  <td>\
						    <div class="form-group">\
						      <input class="form-control required" name="designation'+i+'" type="text">\
						    </div>\
						  </td>\
						  <td>\
						    <div class="form-group">\
						      <input type="date" name="from'+i+'" class="form-control required">\
						    </div>\
						  </td>\
						  <td>\
						    <div class="form-group">\
						      <input type="date" name="to'+i+'" class="form-control required">\
						    </div>\
						  </td>\
						  <td>\
						    <div class="form-group">\
						      <input class="form-control required" name="name_of_company'+i+'" type="text">\
						    </div>\
						  </td>\
						</tr>'
					);
					i++;
        		});

        },

        _addlanguage : function() {

        	var i = 2;

        		$(".addeducationfield").click(function() {
        			$("table tbody tr.add_language").last().after(
			        	'<tr class="add_language">\
						  <input type="hidden" name="count">\
						  <td>\
						    <div class="form-group">\
						      <input class="form-control required" name="language'+i+'" type="text">\
						    </div>\
						  </td>\
						  <td>\
						    <div class="form-group">\
						      <input class="form-control required" name="speaking'+i+'" type="text">\
						    </div>\
						  </td>\
						  <td>\
						    <div class="form-group">\
						      <input class="form-control required" name="listening'+i+'" type="text">\
						    </div>\
						  </td>\
						  <td>\
						    <div class="form-group">\
						      <input class="form-control required" name="reading'+i+'" type="text">\
						    </div>\
						  </td>\
						  <td>\
						    <div class="form-group">\
						      <input class="form-control required" name="writing'+i+'" type="text">\
						    </div>\
						  </td>\
						</tr>'
					);
					i++;
				});
        },

        addcomment : function() {

        	var t = this;

        	$("form#addcomment").submit(function(){

        		var url = $(this).attr('action');

        		$.post( url + '/admin/activity/store',{

        			_token  : $("input[name='_token']").val(),
        			user_id : $("input[name='user_id']").val(),
        			customers_id : $("input[name='customers_id']").val(),
        			activity  : $("textarea[name='activity']").val()

        		}).done(function( data ){

					if( data ) {	

						$("form#addcomment").find("textarea").val("");
                        $("form#addcomment").before('<div class="alert alert-success" id="commentmessage">Successfully Added Comment.</div>');

                        $('div#commentmessage').delay(2000).fadeOut('slow', function(){
			                location.reload(true);
			            });
					}

        		}, "json");

        		return false;
        	});
        },

        addspouse : function() {

        	var t = this;

        	$("form#addspouse").submit(function(){

        		console.log( $(this).serialize() );
        		var isValid = true;

        		$(this).find("span.error").remove();
				
				//validate email
				if ( !t._validateEmail( $("input[name='email']").val() ) ) {
					isValid = false;
				}


				if( t._validateForm( isValid ) ) {

					$.post(	$(this).attr('action') ,
							{

								'_token'  		 : $("input[name='_token']").val(),
								'customers_id'   : $("input[name='customers_id']").val(),
								'name'    		 : $("input[name='name']").val(),
								'email'   		 : $("input[name='email']").val(),
								'date_of_birth'  : $("input[name='date_of_birth']").val(),
								'contact_number' : $("input[name='contact_number']").val(),

					}).done( function( data ){


						if( data != true ) {

							
								// $("#myModal").modal('show');
								// $("#myModal").find('.modal-body').append('Successfully Added Customer Spouse.');

								$("form#addspouse").find("input[type='text'],input[type='email'], input[type='date'], select").val("");
								$("form#addspouse").before('<div class="alert alert-success" id="addspousemessage">Successfully Added Customer Spouse.</div>');
								$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

								$('div#addspousemessage').delay(2000).fadeOut('slow', function(){
					                location.reload(true);
					            });
								

						} 
							else {
								
								$("form#addspouse").before('<div class="alert alert-danger">Email Already Exist!</div>');
							
						}

						console.log( data );
					}, "json");


					
				}

				return false;

        	});

        },

         addspouseeducation: function() {

        	var t = this;

        
        	$("form#addspouseeducation").submit(function(){

        		// console.log( $(this).serialize() );
        		// return false;
        		console.log( $(this).serialize() );
        		var isValid  = true;       	
        		$(this).find("span.error").remove();	
				var educ_len = $("input[name='count']").length;

				for ( var i = 1; i <= educ_len; i++ ) {

					var startDate = new Date( $("input[name='from"+i+"']").val() );
					var endDate   = new Date( $("input[name='to"+i+"']").val() );
					
					if ( startDate > endDate ) {

						$("input[name='from"+i+"']").after('<span class="error">Start Date Must Be Lesser than End Date</span>');
						isValid = false;
					}

				}

				if( t._validateForm( isValid ) ) {
					$.post( $(this).attr('action'), {

						'_token' : $("input[name='_token']").val(),
						count 	 : educ_len,
						data 	 : $( this ).serialize()
						
					}).done(function( data ){

							if( data ) {	
								// $("#myModal").modal('show');
								// $("#myModal").find('.modal-body').append('Successfully Added Customer Spouse Education.');

								$("form#addspouseeducation").find("input[type='text'], input[type='date'], select").val("");
								$("form#addspouseeducation").before('<div class="alert alert-success" id="spouseeducationmessage">Successfully Added Customer Spouse Education.</div>');
								$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

								$('div#spouseeducationmessage').delay(2000).fadeOut('slow', function(){
					                location.reload(true);
					            });
							}
							console.log( data );


					}, "json");
				}
				return false;

        	});
        },

        addspouseworkhistory : function() {

        	var t = this;


        	$("form#addspousework").submit(function(){

        		console.log( $(this).serialize() );
        		var isValid = true;       	
        		$(this).find("span.error").remove();	
				var work_len = $("input[name='count']").length;


				for ( var i = 1; i <= work_len; i++ ) {

					var startDate = new Date( $("input[name='from"+i+"']").val() );
					var endDate   = new Date( $("input[name='to"+i+"']").val() );
					
					if ( startDate > endDate ) {

						$("input[name='from"+i+"']").after('<span class="error">Start Date Must Be Lesser than End Date</span>');
						isValid = false;
					}

				}

				if( t._validateForm( isValid ) ) {
					$.post( $(this).attr('action'), {

						'_token' : $("input[name='_token']").val(),
						count 	 : work_len,
						data 	 : $( this ).serialize()
						
					}).done(function( data ){

							if( data ) {	
								// $("#myModal").modal('show');
								// $("#myModal").find('.modal-body').append('Successfully Added Customer Spouse Work History.');

								$("form#addspousework").find("input[type='text'], input[type='date']").val("");
								$("form#addspousework").before('<div class="alert alert-success" id="spouseworkmessage">Successfully Added Customer Spouse Work History.</div>');
								$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

								$('div#spouseworkmessage').delay(2000).fadeOut('slow', function(){
					                location.reload(true);
					            });
							}
							console.log( data );


					}, "json");
				}
				return false;

        	});

        },

        addspouselanguage : function() {

        	var t = this;


        	$("form#addspouselanguage").submit(function(){

        		console.log( $(this).serialize() );
        		var isValid = true;       		
				$(this).find("span.error").remove();

				if( t._validateForm( isValid ) ) {

					$.post( $(this).attr('action'), {

						'_token' : $("input[name='_token']").val(),
						count 	 : $("input[name='count']").length,
						data 	 : $( this ).serialize()
						
					}).done(function( data ){

							if( data ) {	
								// $("#myModal").modal('show');
								// $("#myModal").find('.modal-body').append('Successfully Added Customer Spouse Language.');

								$("form#addspouselanguage").find("input[type='text']").val("");
								$("form#addspouselanguage").before('<div class="alert alert-success" id="spouselanguagemessage">Successfully Added Customer Spouse Language.</div>');
								$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

								$('div#spouselanguagemessage').delay(2000).fadeOut('slow', function(){
					                location.reload(true);
					            });
							}
							console.log( data );


					}, "json");
				}
				return false;

        	});
        },




        deleteresume : function( id, token ) {

			$.post(  'deleteresume/'+id , {
				'_token' : token,
				success : function( data ) {

					console.log( data );
				}
			});

		},


		_searchname : function() {
			var t = this;
			$("#addlastname").keyup(function(){

				var token = $(this).data('token');
				var url   = $(this).data('url');
				var string = $(this).val();
				
				console.log( token );
				$("select.option_name option").remove();

				if( string != "" ) {

					// $.post( url+'/'+string,{

					// 	_token : token,
					// 	beforeSend: function (xhr) {
				 //            var token = $('meta[name="_token"]').attr('content');

				 //            if (token) {
				 //                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
				 //            }
				 //        },

					// 	error : function( error ) {
					// 		console.log( error );
					// 	}
					// }).done(function( data ){

					// 	if( typeof( data ) != 'undefined' ) {

					// 		var obj = [];
					// 		$.each( data, function( key, value ) {					
					// 			obj.push( value.lastname + ' ' + value.firstname );
					// 		});

					// 		$("#addlastname").autocomplete({
				 //                    source: obj
				 //             });

					// 	}


					// }, "json");

					$.ajax({
						url : url+'/'+string,
						type : "POST",
						dataType : "json",
						processData: false,
						data : { _token : token },
						beforeSend: function (xhr) {
				            var token = $('meta[name="_token"]').attr('content');

				            if (token) {
				                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
				            }
				        },

					}).done(function( data ){

						if( typeof( data ) != 'undefined' ) {

							var obj = [];
							$.each( data, function( key, value ) {					
								obj.push( value.lastname + ' ' + value.firstname );
							});

							$("#addlastname").autocomplete({
				                    source: obj
				             });

						}


					});
				}
			});

		},

		editlanguage : function() {

			var t = this;
			$("form#editcustomerslanguage").submit(function(){

				var isValid = true;

				if ( t._validateForm( isValid ) ) {
					return true;
				}

				else {
					return false;
				}

			});
		},


		_approvecustomer : function() {

			var t = this;


			$("#approve").click(function(){

				var id 		= $(this).data('id');
				var _token  = $(this).data('token');
				var url 	= $(this).data('url');

				$.post( url+'/admin/customers/approve/'+id, {

					'_token' : _token,

				}).done(function( data ){

					if( data ) {	
						$("#approve").remove();
						$("#disapprove").remove();
						$(".changestatsmessage").append('<div class="alert alert-success" id="approveustomermessage">Successfully Approve Customer.</div>');
						$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

						$('div#approveustomermessage').delay(2000).fadeOut('slow', function(){
			                location.reload(true);
			            });
					}
					console.log( data );
					
				}, "json");
			});

		},

		_disapprovecustomer : function() {

			var t = this;

			$("#disapprove").click(function(){

				var id 		= $(this).data('id');
				var _token  = $(this).data('token');
				var url 	= $(this).data('url');

				$.post( url+'/admin/customers/disapprove/'+id, {

					'_token' : _token,

				}).done(function( data ){

					if( data ) {	
						$("#approve").remove();
						$("#disapprove").remove();
						$(".changestatsmessage").append('<div class="alert alert-success" id="disapproveustomermessage">Successfully Disapprove Customer.</div>');
						$('html,body').animate({scrollTop: $("#top").offset().top},'slow');

						$('div#disapproveustomermessage').delay(2000).fadeOut('slow', function(){
			                location.reload(true);
			            });
					}
					console.log( data );
					
				}, "json");
			});

		},

		_holdcustomer : function() {
			var t = this;

			$("#holdcustomer").click(function(){

				var id 		= $(this).data('id');
				var _token  = $(this).data('token');
				var url 	= $(this).data('url');

				$.post( url+'/admin/customers/hold/'+id, {

					'_token' : _token,

				}).done(function( data ){

					if( data ) {	
						
						$(".changestatsmessage").append('<div class="alert alert-success" id="holdcustomermessage">Successfully Hold Customer.</div>');
						$('html,body').animate({scrollTop: $("#top").offset().top},'slow');
						$('div#holdcustomermessage').delay(2000).fadeOut('slow', function(){
			                location.reload(true);
			            });
					}
					console.log( data );
					
				}, "json");

			});
		},

		_unholdcustomer : function() {
			var t = this;
			$("#unholdcustomer").click(function(){

				var id 		= $(this).data('id');
				var _token  = $(this).data('token');
				var url 	= $(this).data('url');

				$.post( url+'/admin/customers/unhold/'+id, {

					'_token' : _token,

				}).done(function( data ){

					if( data ) {	
						
						$(".changestatsmessage").append('<div class="alert alert-success" id="unholdcustomermessage">Successfully Unhold Customer.</div>');
						$('html,body').animate({scrollTop: $("#top").offset().top},'slow');
						$('div#unholdcustomermessage').delay(2000).fadeOut('slow', function(){
			                location.reload(true);
			            });
					}
					console.log( data );
					
				}, "json");

			});
		},

        _validateForm : function( isValid ) {

			var t    	= this;
			//var isValid = true;
			var form 	= $("form");

			// form.find(".error").remove();

			form.find('.required').each(function(){

				if( $(this).val() == "" ) {

					$(this).after('<span class="error">'+ t.error +'</span>');
					isValid = false;
				} 				

			});

			form.find('.number').each(function(){

				if( $(this).val() != "" ) {

					if( !t.isNumeric( $(this).val() ) ) {

						$(this).after('<span class="error">'+ t.number +'</span>');
						isValid = false;

					}

				}

			});

			form.find('input[type="email"]').each(function(){

				if( $(this).val() != "" ) {

					if( !t._validateEmail( $(this).val() ) ) {
						$(this).after('<span class="error">'+ t.error +'</span>');
						isValid = false;
					}

				}
			});

			
			return isValid;
		},

		isNumeric: function( obj ) {
		    return !$.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
		},

		_numtype : function() {

			$("select[name='primarynumtype']").change(function(){
				
				if( $(this).val() == 'm' ) 
					$("input[name='contact_number']").mask("(9999) 999-9999",{placeholder: 'X'});

				else 
					$("input[name='contact_number']").mask("(999)-9999",{placeholder: 'X'});

			});

			$("select[name='secondarynumtype']").change(function(){
				
				if( $(this).val() == 'm' ) 
					$("input[name='secondary_contact_number']").mask("(9999) 999-9999",{placeholder: 'X'});

				else 
					$("input[name='secondary_contact_number']").mask("(999)-9999",{placeholder: 'X'});

			});

			$("select[name='othernumtype']").change(function(){
				
				if( $(this).val() == 'm' ) 
					$("input[name='other_contact_number']").mask("(9999) 999-9999",{placeholder: 'X'});

				else 
					$("input[name='other_contact_number']").mask("(999)-9999",{placeholder: 'X'});

			});

		},

       

        /*
        *   @function : Validate Email
        *   @params   : email
        *
        */
        _validateEmail : function( email ) {

            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

            return pattern.test(email);

        },

        _wait : function( ms ) {
            var defer = $.Deferred();
            setTimeout(function() { defer.resolve(); }, ms);
            return defer;
        },

        _urlParams : function ( url ) {

            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for ( var i = 0; i < sURLVariables.length; i++ ) {

                var sParameterName = sURLVariables[i].split('=');

                if ( sParameterName[0] == url ) {
                    return sParameterName[1];
                }
            }
        },

        _countrefresh : function () {


			var url = global_url+"/admin/countactivity";
				
			$.post(  url , {}).done(function( data ){
				return $("#activity_count").text(data);
			});

    	},

        /**
         * helper: _evalData
         * @param {Object} D
         */
        _evalData : function(D) {
            return eval('(' + D + ')');
        }
    };
})(jQuery);
$(document).ready(function() {
    $.customers.init();  
});

