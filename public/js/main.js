/**
 * Main Javascript For Juab Title
 * all javascript functionality
 * extend Report
 * Author : Angelo
 * v 0.0.1.0
 **/

;(function($) {
    $.main = {
        error  :  'This field is required.',
        number : 'Invalid Number',
        /**
         * function : init
         */
        init : function() {
             
            this._numtype();
            this.deleteuser();
            this.readall();
            this.printtable();
            this.deletecustomers();
            this.exportpdf();
            this.addpayment();
            this.adddocuments();
            this.hasdocument();

            if( $("#filters").val() == null || typeof( $("#filters").val() ) == undefined || $("#filters").val() == "" ) {
                var cust_table = $('#customers').dataTable({
                    "ordering": false,
                    "oSearch": {"bSmart": false}
                }).columnFilter({
                    sPlaceHolder: "head:before",
                    aoColumns: [ 
                        null,
                        null,
                        null,
                        null,                                    
                        { type : "select" },               
                        { type : "select" }
                       
                    ]
                });
            } 

            else {

                var filter = $("#filters").val();

                var cust_table = $('#customers').dataTable({
                    "ordering": false,
                    "oSearch": {"bSmart": false}
                }).columnFilter({
                    sPlaceHolder: "head:before",
                    aoColumns: [ 
                        null,
                        null,
                        null,
                        null,                                    
                        { type : "select",selected: $("#filter_name").val() },               
                        { type : "select" }
                       
                    ]
                });

            }

            $('#logs').dataTable({
                "ordering": false,
                "oSearch": {"bSmart": false}
            }).columnFilter({
                sPlaceHolder: "head:before",
                aoColumns: [ 
                    null,
                    null,
                    null,
                    null,                                  
                    { type : "select" },
                ]
            });

            $(".select_filter").change(function(){
                if( $(this).val() != "" ){
                    cust_table.fnFilter("^"+$(this).val()+"$", 5, true );
                }
            });
            

            // $('#activity').dataTable({"ordering": false});
            $('#users').dataTable({"ordering": false});

            $('#customers-activity').dataTable({"ordering": false});

            $('#customerpayments').dataTable({"ordering": false});

            if( this._urlParams('delete_user') != null ) {
                $(".user-deleted").append('<div class="alert alert-success">Successfully Deleted User!</div>');
            }
            if( this._urlParams('delete_customer') != null ) {
                $(".customer-deleted").append('<div class="alert alert-success">Successfully Deleted Customer!</div>');
            }
             if( this._urlParams('add_payment') != null ) {
                $("#addpayment").append('<div class="alert alert-success">Successfully Add Payment!</div>');
            }

        },

        _numtype : function() {

            $("select[name='num_type']").change(function(){
                
                if( $(this).val() == 'm' ) 
                    $("input[name='contactnumber']").mask("(9999) 999-9999",{placeholder: 'X'});

                else 
                    $("input[name='contactnumber']").mask("(999)-9999",{placeholder: 'X'});

            });
        },
        
        deleteuser : function() {

            var t = this;

             $('#deleteuser').on('hidden.bs.modal', function () {
                location.reload();
            });

            $(".delete-user").click(function(){

                var path  = $(this).data('url');
                var token = $(this).data('token');

                $.post(  path ,{ 
                    _method: 'delete', _token : $(this).data('token')
                } ).done( function( data ) {

                    if( data ) {  
                            
                       //$('#deleteuser').modal('show');


                        $("span.user-deleted").append('<div class="alert alert-success" id="spouseworkmessage">Successfully Added Customer Spouse Work History.</div>');
                        $('html,body').animate({scrollTop: $("#top").offset().top},'slow');

                        $('div#spouseworkmessage').delay(2000).fadeOut('slow', function(){
                            location.reload(true);
                        });
                    } else {
            
                        location.reload( true );
                    }
                     console.log( data );

                }, "json");

                return false;
            });
        },
        readall : function() {
            var t = this;
            $(".readall").click(function(){
                
                var url   = $(this).data('url');
                var token = $(this).data('token');

                $.post( url + '/admin/activitylog', {
                    _token : token
                        }).done( function( data ){
                                $("#logs").find("tbody tr").removeClass('bold');
                            }, "json");
            });

        },

        exportpdf : function() {
            // var doc = new jsPDF('landscape');
            // var source = $('#customers')[0];
            // var specialElementHandlers = {
            //     '#editor': function (element, renderer) {
            //         return true;
            //     }
            // };
            //  margins = {
            //     top: 80,
            //     bottom: 60,
            //     left: 40,
            //     width: 522
            // };
            // $("#exportpdf").click(function(){

            //     doc.fromHTML(
            //         source, 
            //         15, 
            //         15, {
            //             'width': margins.width,
            //             'elementHandlers': specialElementHandlers
            //     });
              
            //         doc.save('sample-file.pdf');
                
            // });
        },

        deletecustomers : function() {

            var t = this;

            $('#deletecustomer').on('hidden.bs.modal', function () {
                location.reload();
            });
            
            $(".custdelete").click(function(){

                var path  = $(this).data('url');
                var token = $(this).data('token');

                $.post(  path ,{ 
                    _method: 'delete', _token : token
                } ).done( function( data ) {

                    if( data ) {  
                            
                        //$('#deletecustomer').modal('show');
                        $(this).parents("tr").remove();
                        $(".deletecustmessage").append('<div class="alert alert-success" style="display: inline-block;width: 100%;" id="deletecustomermessage">Successfully Deleted Customer!</div>');
                        $('html,body').animate({scrollTop: $("#top").offset().top},'slow');

                        $('div#deletecustomermessage').delay(2000).fadeOut('slow', function(){
                            location.reload(true);
                        });

                    } 
                     console.log( data );

                }, "json");

                return false;
            });
        },


        addpayment : function () {

            var t = this;
            $("input[name='amount']").keypress(function(key){
                if(key.charCode < 48 || key.charCode > 57) return false;
            });

            $("input[name='amount']").blur(function()
            {
                $("input[name='amount']").formatCurrency({
                    symbol: '₱',
                });
            });

            $('#myModal').on('hidden.bs.modal', function () {
                location.reload();
            });

            $("#addpayment").submit(function(){

                var url             = $(this).attr('action');
                var type            = $("select[name='payment_type']");
                var amount          = $("input[name='amount']");
                var token           = $("input[name='_token']").val();
                var customers_id    = $("input[name='customers_id']").val();
                var user_id         = $("input[name='user_id']").val();
                var isValid = true;


                 if( type.val() == "" ) {

                    type.after('<span class="error">'+ t.error +'</span>');
                    isValid = false;
                }   

                if( isValid ){

                    $.post( url, {
                        _token       : token,
                        customers_id : customers_id,
                        type         : type.val(),
                        amount       : amount.val(),
                        user_id      : user_id,
                    }).done(function(data){

                        if( data ) {  
                            
                          // $("#myModal").modal('show');
                          // $("#myModal").find('.modal-body').append('Successfully Added Payment.');

                            $("form#addpayment").find("input[type='text'], select").val("");
                            $("form#addpayment").before('<div class="alert alert-success" id="addpaymentmessage">Successfully Added Payment.</div>');
                            $('html,body').animate({scrollTop: $("#top").offset().top},'slow');

                             $('div#addpaymentmessage').delay(2000).fadeOut('slow', function(){
                                location.reload(true);
                            });
                        } 

                        console.log( data );
                    }, "json");
                }

                return false;
            });
        },

        adddocuments : function() {

            var t = this;
            $('#adddocument').on('hidden.bs.modal', function () {
                location.reload();
            });

            $("#addcustdoc").submit(function(){

                var title           = $(this).find("input[name='title']").val();
                var description     = $(this).find("textarea[name='description']").val();
                var application_id  = $(this).find("select[name='application_id']").val();
                var url             = $(this).attr("action");
                var token           = $(this).find("input[name='_token']").val();

                $.post( url, {

                    _token : token,
                    title  : title,
                    description : description,
                    application_id : application_id

                }).done(function( data ){

                    if( data ) {

                        $("form#addcustdoc").find("input[type='text'],textarea,select").val("");
                        $("form#addcustdoc").before('<div class="alert alert-success" id="adddocumentmessage">Successfully Added Document.</div>');
                        $('html,body').animate({scrollTop: $("#top").offset().top},'slow');

                        $('div#adddocumentmessage').delay(2000).fadeOut('slow', function(){
                            location.reload(true);
                        });
                    }
                    console.log( data );

                }, "json");


                return false;
            });

        },

        hasdocument : function() {

            var t = this;
             $('#hasdocmodal').on('hidden.bs.modal', function () {
                location.reload();
            });

            $("input[name='customers_docs']").on('click', function(){
                var url = $(this).data('url');
                var token = $(this).data('token');
                var customers_id = $(this).data('customersid');
                var documents_id = $(this).data('documentsid');

                $.post( url, {
                    _token : token,
                    customers_id : customers_id,
                    documents_id : documents_id
                }).done(function( data ){

                    if( data == 1 ){
                        $('#hasdocmodal').find(".modal-body").text('Successfully Added Document.');
                        $('#hasdocmodal').modal('show');
                    }
                    else {
                         $('#hasdocmodal').find(".modal-body").text('Successfully Removed Document.');
                         $('#hasdocmodal').modal('show');
                    }
                    console.log( data );
                }, "json");

                return false;
            });

        },

        printtable : function() {
            var t = this;
            $("#print").click(function(){
                $("#customers").find(".action").remove();
                t._printdata();
            });
             
        },
        _validateForm : function( ) {

            var t       = this;
            var isValid = true;
            var form    = $("form");

            form.find('.required').each(function(){

                if( $(this).val() == "" ) {

                    $(this).after('<span class="error">'+ t.error +'</span>');
                    isValid = false;
                }               

            });

            form.find('.number').each(function(){

                if( $(this).val() != "" ) {

                    if( !$.isNumeric( $(this).val() ) ) {

                        $(this).after('<span class="error">'+ t.number +'</span>');
                        isValid = false;

                    }

                }

            });

            form.find('input[type="email"]').each(function(){

                if( $(this).val() != "" ) {

                    if( !t._validateEmail( $(this).val() ) ) {
                        $(this).after('<span class="error">'+ t.error +'</span>');
                        isValid = false;
                    }

                }
            });

            
            return isValid;
        },

        _printdata : function() {

           var customers = document.getElementById("customers");
           win = window.open("");
           win.document.write(customers.outerHTML);
           win.print();
           win.close();
           location.reload();
        },
        /*
        *   @function : Validate Email
        *   @params   : email
        *
        */
        _validateEmail : function( email ) {

            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

            return pattern.test(email);

        },

        _wait : function( ms ) {
            var defer = $.Deferred();
            setTimeout(function() { defer.resolve(); }, ms);
            return defer;
        },

        _urlParams : function ( url ) {

            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for ( var i = 0; i < sURLVariables.length; i++ ) {

                var sParameterName = sURLVariables[i].split('=');

                if ( sParameterName[0] == url ) {
                    return sParameterName[1];
                }
            }
        },

        /**
         * helper: _evalData
         * @param {Object} D
         */
        _evalData : function(D) {
            return eval('(' + D + ')');
        }
    };
})(jQuery);
$(document).ready(function() {
    $.main.init();  
});