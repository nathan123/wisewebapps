<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	protected $table = 'customers';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = [

			'applications_id',
			'source',
			'user_id',
			'firstname',
			'lastname',
			'middle_name',
			'date_of_birth',
			'gender',
			'contact_number',
			'secondary_contact_number',
			'other_contact_number',
			'email',
			'secondary_email',
			'other_email',
			'city',
			'province',
			'country',
			'profession',
			'is_read',
			'meta_description_first',
			'meta_description_second'

	];

	/**
	 * The attribute Customer Family Composition.	
	 *
	 * @param object
	 */
	public function user() {

		return $this->belongsTo('App\User');
	}

	/**
	 * The attribute Customer Family Composition.	
	 *
	 * @param object
	 */

	public function family() {

		return $this->hasOne('App\CustomersFamilyComposition');
	}

	/**
	 * The attribute Customer Education.
	 *
	 * @param object
	 */

	public function education() {

		return $this->hasMany('App\CustomersEducation');
	}

	/**
	 * The attribute Customer Work History.
	 *
	 * @param object
	 */

	public function work() {

		return $this->hasMany('App\CustomersWorkHistory');
	}

	/**
	 * The attribute Customer Language.
	 *
	 * @param object
	 */

	public function language() {
		
		return $this->hasMany('App\CustomersLanguage');
	}

	/**
	 * The attribute Customer Spouse.
	 *
	 * @param object
	 */

	public function spouse() {
		return $this->hasOne('App\CustomersSpouse');
	}

	/**
	 * The attribute Customer File.
	 *
	 * @param object
	 */

	public function file() {
		return $this->hasOne('App\CustomersFile');
	}

	/**
	 * The attribute Customer Additional Info.
	 *
	 * @param object
	 */

	public function info() {
		return $this->hasMany('App\CustomersAdditionalInfo');
	}
	

	public function checkname( $firstname, $lastname ) {

		$check = Customers::where( 'firstname', '=', $firstname )->where( 'lastname', '=', $lastname )->first();
		if( count( $check ) > 0 ) {
			return true;
		}

		return false;

	}

	public function application() {
		return $this->hasOne('App\Application');
	}

	public function documents() {

    	return $this->belongsToMany('App\Documents' )->withPivot( 'customers_id', 'documents_id', 'is_available' );
    	
	}

}
