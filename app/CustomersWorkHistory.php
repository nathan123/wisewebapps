<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersWorkHistory extends Model {

	protected $table = 'customers_work_histories';
	
	protected $fillable = [
			'customers_id',
			'from',
			'to',
			'name_of_company',
			'designation'
	];

	public function customers() {
		return $this->belongsTo('App\Customers');
	}

}
