<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersSpouseLanguage extends Model {

	protected $table = 'customers_spouse_languages';
	
	protected $fillable = [
			'spouses_id',
			'language',
			'speaking',
			'listening',
			'reading',
			'writing'
	];

	public function customers() {
		return $this->belongsTo('App\CustomersSpouse');
	}

}
