<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model {

	protected $table = 'documents';
	
	protected $fillable = [
			'title',
			'description',
			'applications_id'
	];


	public function customers() {
		return $this->belongsToMany('App\Customers');
	}

	public function application() {
		return $this->belongsTo('App\Application', 'applications_id');
	}
}
