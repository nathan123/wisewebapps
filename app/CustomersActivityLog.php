<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class CustomersActivityLog extends Model {

	protected $table = 'customers_activity_logs';

	protected $fillable = [
			'customers_id',
			'activity',
			'is_read',
			'user_id',
			'editor'
	];

	public function customers() {
		return $this->belongsTo('App\Customers');
	}

	public function user() {
		return $this->belongsTo('App\User');
	}

}
