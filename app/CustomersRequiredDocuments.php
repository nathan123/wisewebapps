<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersRequiredDocuments extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public $timestamps = false;

	protected $table = 'customers_documents';
	
	protected $fillable = [
			'customers_id',
			'documents_id',
			'applications_id',
			'is_available'
	];

	public function customers() {

		return $this->belongsToMany('App\Customers');
	}

	public function documents() {
		
		return $this->belongsTo('App\Documents');
	}

	public function application() {
		
		return $this->belongsTo('App\Application');
	}
}
