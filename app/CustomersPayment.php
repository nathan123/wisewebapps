<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersPayment extends Model {

	protected $table = 'customers_payments';
	
	protected $fillable = [
			'customers_id',
			'user_id',
			'title',
			'amount',
			'description',
			'meta_option_1',
			'meta_option_2',
			'meta_option_3'
	];

	public function customers() {
		return $this->belongsTo('App\Customers');
	}

	public function user() {
		return $this->belongsTo('App\User');
	}

}
