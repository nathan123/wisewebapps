<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersSpouseEducation extends Model {

	protected $table = 'customers_spouse_educations';

	protected $fillable = [
			'spouses_id',
			'status',
			'from',
			'to',
			'name_of_school',
			'qualification'
	];


	public function customersspouse() {
		return $this->belongsTo('App\CustomersSpouse');
	}

}
