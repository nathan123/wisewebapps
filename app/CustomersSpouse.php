<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersSpouse extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	protected $table = 'customers_spouses';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = [
			'customers_id',
			'name',
			'date_of_birth',
			'contact_number',
			'email'
	];


	/**
	 * The attribute Customers Spouse.
	 *
	 * @param object
	 */

	public function customers() {
		return $this->belongsTo('App\Customers');
	}

	/**
	 * The attribute Customers Spouse Education.
	 *
	 * @param object
	 */

	public function education() {

		return $this->hasMany('App\CustomersSpouseEducation');
	}

	/**
	 * The attribute Customers Spouse Work History.
	 *
	 * @param object
	 */

	public function work() {

		return $this->hasMany('App\CustomersSpouseWorkHistory');
	}

	/**
	 * The attribute Customers Spouse Language.
	 *
	 * @param object
	 */

	public function language() {
		
		return $this->hasMany('App\CustomersSpouseLanguage');
	}

}
