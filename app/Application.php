<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	protected $table = 'applications';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = [

		'title',
		'description',

	];

	public function documents() {
		return $this->hasMany('App\Documents');
	}
}
