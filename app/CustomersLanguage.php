<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersLanguage extends Model {

	protected $table = 'customers_languages';
	
	protected $fillable = [
			'customers_id',
			'language',
			'speaking',
			'listening',
			'reading',
			'writing'
	];

	public function customers() {
		return $this->belongsTo('App\Customers');
	}

}
