<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersAdditionalInfo extends Model {

	protected $table = 'customers_additional_infos';


	protected $fillable = [
			'customers_id',
			'key',
			'value'
	];
	

	public function customers() {
		return $this->belongsTo('App\Customers');
	}

}
