<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::filter('get_user_role', function () {

	if( Auth::check() ) {

	    $user  = App\User::with('role')->find( Auth::user()->id );
	    $roles = $user->roles;
	    \View::share('role', $roles);
	    
	    if( Auth::user()->hasRole('admin') || Auth::user()->hasRole('receptionist') ||  Auth::user()->hasRole('encoder') ){

	    	$cust 	    	= App\Customers::where('is_read', '=', 0)->count();
	    	$approve    	= App\Customers::where('status', '=', 'approve')->count();	                        
	    	$disapprove 	= App\Customers::where('status', '=', 'disapprove')->count(); 
	    	$activity  		= App\CustomersActivityLog::count();
	    	$customers_cnt  = App\Customers::all()->count();
	    	$users_cnt  	= App\User::all()->count();                       
	    }

	    else {
	    	$cust 			= App\Customers::where('user_id', '=', Auth::user()->id )
		                        ->where('is_read', '=', 0)
		                        ->count();

		    $approve    	= App\Customers::where('status', '=', 'approve')
		    					->where('user_id', '=', Auth::user()->id )
		    					->count();	 

	    	$disapprove 	= App\Customers::where('status', '=', 'disapprove')
	    						->where('user_id', '=', Auth::user()->id )
	    						->count();

	    	$activity  		= App\CustomersActivityLog::where('user_id', '=', Auth::user()->id )
	    						->count();

	    	$customers_cnt  = App\Customers::where('user_id', '=', Auth::user()->id )->count();
	    	$users_cnt  	= App\User::all()->count();
	    }
		    
		\View::share('count', $cust);
		\View::share('approve', $approve);
		\View::share('disapprove', $disapprove);
		\View::share('activity_count', $activity);
		\View::share('customers_cnt', $customers_cnt);
		\View::share('users_cnt', $users_cnt);

    }

    else {
    	return redirect('/auth/login');
    }
});


if( Auth::check() ) {
	Route::get('/', 'Auth\AdminController@index');
}
	
else {
	Route::get('/', 'Auth\AuthController@getLogin');
	
}


Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'before' => 'get_user_role' ], function () {

	#export
	Route::get('customers/exportexcel', function(){

		#$data = array();		
		\Excel::create('customers', function($excel) {

			if ( Auth::user()->hasRole('case_officer') ) {
				$customer = App\Customers::with('user')->where('user_id', '=', Auth::user()->id )->get();
			}
			else {
				$customer = App\Customers::with('user')->get();
			}

			$data[] = array( 'Name', 'Email', 'Gender', 'Contact Number', 'Status', 'Case Officer' );
			foreach ($customer as $key => $value) {

				$data[] = array( ucwords( $value->firstname . ' ' . $value->lastname .' ' . $value->middle_name ), $value->email, $value->gender, $value->contact_number, $value->status, $value->user->name );
			}

			
			// Manipulate first row
			$excel->sheet('Sheetname', function($sheet) use($data) {

		        $sheet->fromArray($data, null, 'A1', false, false);

			        // Set black background
					$sheet->row(1, function($row) {

					    // call cell manipulation methods
					    $row->setBackground('#e8e8e8');

					});

				$sheet->cell('A1', function($cell) {

				  $cell->setBorder(array(
					    'borders' => array(
					        'top'   => array(
					            'style' => 'solid'
					        ),
					    )
					));

				});
		    });

		})->store('xls', storage_path('app'));
		$file = storage_path(). '\app\customers.xls';
		return Response::download($file, 'customers_'.date('Y-md-d').'.xls', ['content-type' => 'application/vnd.ms-excel']);

	});

	Route::get('customers/exportpdf', function(){

		if ( Auth::user()->hasRole('case_officer') ) {
			$customer = App\Customers::with('user')->where('user_id', '=', Auth::user()->id )->get();
		}
		else {
			$customer = App\Customers::with('user')->get();
		}
		$html 	  = view('export.exportpdf', compact('customer'));

    	return \PDF::load($html, 'A4', 'landscape')->filename('customers_'. date('Y-md-d') .'.pdf')->download();
	});

	Route::get('dashboard', 'Admin\AdminController@index');
	Route::get('activity','Admin\AdminController@activity');
	Route::post('activitylog','Admin\AdminController@activitylog');
	Route::post('activity/store', 'Admin\AdminController@storeActivity');

	Route::resource('customers', 'Admin\CustomersController');
	Route::post('customers/store','Admin\CustomersController@store');
	Route::post('customers/deleteresume/{id}','Admin\CustomersController@deleteresume');
	Route::post('customers/approve/{id}', 'Admin\CustomersController@approve');
	Route::post('customers/disapprove/{id}', 'Admin\CustomersController@disapprove');

	Route::post('customers/hold/{id}', 'Admin\CustomersController@hold');
	Route::post('customers/unhold/{id}', 'Admin\CustomersController@unhold');

	#Family
	Route::get('customers/addfamilycomposition/{id}', 'Admin\CustomersController@addfamily');
	Route::post('customers/storefamily/{customers_id}', 'Admin\CustomersController@storefamily');
	Route::resource('customers/family', 'Admin\CustomersFamilyController');

	#Education
	Route::get('customers/addeducation/{id}', 'Admin\CustomersController@addeducation');
	Route::post('customers/storeeducation', 'Admin\CustomersController@storeeducation');
	Route::resource('customers/education', 'Admin\CustomersEducationController');

	#Work
	Route::get('customers/addwork/{id}', 'Admin\CustomersController@addwork');
	Route::post('customers/storework', 'Admin\CustomersController@storework');
	Route::resource('customers/work', 'Admin\CustomersWorkHistoryController');

	#Language
	Route::get('customers/addlanguage/{id}', 'Admin\CustomersController@addlanguage');
	Route::post('customers/storelanguage', 'Admin\CustomersController@storelanguage');
	Route::resource('customers/language', 'Admin\CustomersLanguageController');

	#Spouse
	Route::get('customers/addspouse/{id}', 'Admin\CustomersController@addspouse');
	Route::post('customers/storespouse', 'Admin\CustomersController@storespouse');
	Route::resource('customers/spouse','Admin\CustomersSpouseController');

	#spouseeducation
	Route::get('customers/addspouseeducation/{id}/{customers_id}', 'Admin\CustomersController@addspouseeducation');
	Route::post('customers/storespouseeducation', 'Admin\CustomersController@storespouseeducation');
	Route::resource('customers/spouse/education', 'Admin\CustomersSpouseEducationController');

	Route::get('customers/addspousework/{id}/{customers_id}', 'Admin\CustomersController@addspousework');
	Route::post('customers/storespousework', 'Admin\CustomersController@storespousework');
	Route::resource('customers/spouse/work', 'Admin\CustomersSpouseWorkHistoryController');

	Route::get('customers/addspouselanguage/{id}/{customers_id}', 'Admin\CustomersController@addspouselanguage');
	Route::post('customers/storespouselanguage', 'Admin\CustomersController@storespouselanguage');
	Route::resource('customers/spouse/language', 'Admin\CustomersSpouseLanguageController');

    Route::post('customers/uploadresume', 'Admin\CustomersController@upload');
    Route::post('customers/hasdocument', 'Admin\CustomersController@hasdocument');
    Route::post('customers/removedocument', 'Admin\CustomersController@removedocument');

	Route::resource('users', 'Admin\UsersController');
	Route::get('users/password/{id}', 'Admin\UsersController@password');
	Route::post('users/changepassword/{id}', 'Admin\UsersController@changepassword');
	Route::post('users/store', 'Admin\UsersController@store');

	Route::post('customers/searchname/{string}', 'Admin\CustomersController@searchname');

	Route::post('customers/addpayment', 'Admin\CustomersController@addpayment');

	Route::get('documents','Admin\UsersController@documents');
	Route::post('storedocument', 'Admin\UsersController@storedocument');

	Route::post('countactivity', 'Admin\AdminController@countactivity' );
});


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
