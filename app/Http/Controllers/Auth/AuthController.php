<?php namespace App\Http\Controllers\Auth;

use App\User;
use App\Categories;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

	public function postRegister() {
        
		$rules = [
            'name' 		=> 'required|min:6|unique:users',
            'email' 	=> 'required|email|unique:users',
            'password' 	=> 'required|confirmed|min:6',
            'role'           => 'required'
        ];

        $input = Input::only(
            'name',
            'email',
            'password',
            'password_confirmation',
            'role'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $new_user = new User();
        $new_user->name 	= Input::get('name');
        $new_user->email 	= Input::get('email');
        $new_user->password = \Hash::make( Input::get('password') );
        $new_user->contact_number = Input::get('contactnumber');

        $new_user->save();

        $new_user->roles()->attach(Input::get('role'));

        #--- Handles Email ---#
        // Mail::send('emails.verify', [ 'confirmation_code' => $confirmation_code ], function($message) {
        //     $message->to(Input::get('email'), Input::get('name'))
        //         ->subject('Verify your email address');
        // });

        // \Session::flash('success','Thanks for signing up! Please check your email.');

        return redirect('/');
	}
	
    public function postLogin(Request $request) {

        $remember = true;

        $auth = Auth::attempt( [
                'email'     => Input::get('email'),
                'password'  => Input::get('password')
            ],  $remember
        );

        if ($auth) {

            return redirect('admin/dashboard');
        } else {

           return redirect('auth/login')->withErrors([
                'email' => 'Sorry, unrecognized username or password. Please check and try again.',
            ]);
        }

        // if ($this->auth->attempt($request->only('email', 'password')))
        // {
        //     return redirect('admin/dashboard');
        // }
 
        // return redirect('auth/login')->withErrors([
        //     'email' => 'The credentials you entered did not match our records. Try again?',
        // ]);
    }
}
