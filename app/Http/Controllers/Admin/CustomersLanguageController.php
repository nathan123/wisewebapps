<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CustomersLanguage;
use Illuminate\Http\Request;
use App\Customers;
use Auth;
use App\CustomersActivityLog;

class CustomersLanguageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$custlanguage = CustomersLanguage::find($id);
		return  view('admin.editlanguage',compact('custlanguage'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$custlanguageUpdate = \Request::all();
		$custlanguage   	  = CustomersLanguage::find($id);		
		$custlanguage->update($custlanguageUpdate);

		$cust_rec = Customers::find($custlanguage->customers_id);

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $cust_rec->user_id;
		$customersactivitylog->customers_id = $cust_rec->id;
		$customersactivitylog->activity     = 'Successfully Updated Customers Language';
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();

		 return redirect()->back()->with('success', 'Successfully Edited Language Information!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
