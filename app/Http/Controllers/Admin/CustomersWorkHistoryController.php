<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomersWorkHistory;
use App\Customers;
use Auth;
use App\CustomersActivityLog;


class CustomersWorkHistoryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$custwork = CustomersWorkHistory::find($id);
		return  view('admin.editwork',compact('custwork'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$custworknUpdate = \Request::all();
		$custwork   	  = CustomersWorkHistory::find($id);		
		$custwork->update($custworknUpdate);

		$cust_rec = Customers::find($custwork->customers_id);

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $cust_rec->user_id;
		$customersactivitylog->customers_id = $cust_rec->id;
		$customersactivitylog->activity     = 'Successfully Updated Customers Work History';
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();

		 return redirect()->back()->with('success', 'Successfully Edited Work History Information!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
