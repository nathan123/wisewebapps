<?php namespace App\Http\Controllers\Admin;

use App\CustomersSpouse;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CustomersSpouseWorkHistory;
use Illuminate\Http\Request;
use App\Customers;
use Auth;
use App\CustomersActivityLog;


class CustomersSpouseWorkHistoryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$custspousework = CustomersSpouseWorkHistory::find($id);
		$cust_id 		  = CustomersSpouse::where('id', '=', $custspousework->customers_spouse_id)->pluck('customers_id');
		return  view('admin.editspousework',compact('custspousework','cust_id'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$custspouseworknUpdate = \Request::all();
		$custspousework   	  = CustomersSpouseWorkHistory::find($id);		
		$custspousework->update($custspouseworknUpdate);

		$cust_spouse_rec = CustomersSpouse::find($custspousework->customers_spouse_id);
		$cust_rec 		 = Customers::find($cust_spouse_rec->customers_id);

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $cust_rec->user_id;
		$customersactivitylog->customers_id = $cust_rec->id;
		$customersactivitylog->activity     = 'Successfully Updated Customers Spouse Work History';
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();

		 return redirect()->back()->with('success', 'Successfully Edited Spouse Work History Information!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
