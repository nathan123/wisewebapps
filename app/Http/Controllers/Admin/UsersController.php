<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Application;
use App\Documents;
use App\Customers;
use App\CustomersRequiredDocuments;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller {

	public function __construct() {
		
		$this->middleware('roles');		
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::orderBy('created_at', 'DESC')->get();
        return view('admin.userslist', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.addusers');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = [
            'name' 		=> 'required|min:6|unique:users',
            'email' 	=> 'required|email|unique:users',
            'password' 	=> 'required|confirmed|min:6',
            'role'           => 'required'
        ];

        $input = Input::only(
            'name',
            'email',
            'password',
            'password_confirmation',
            'role'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $new_user 			= new User();
        $new_user->name 	= Input::get('name');
        $new_user->email 	= Input::get('email');
        $new_user->password = \Hash::make( Input::get('password') );
        $new_user->contact_number = Input::get('contactnumber');

        $new_user->save();

        $new_user->roles()->attach(Input::get('role'));
        return redirect()->back()->with('success', 'Successfully Added User!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		return view('admin.editusers', compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{	

		$usersUpdate = \Request::all();
		$user   	 = User::find($id);

        $user->name 			= $usersUpdate['name'];
        $user->email 			= $usersUpdate['email'];
        $user->contact_number 	= $usersUpdate['contactnumber'];

        $user->save();

        $user->roles()->detach();

        $user->roles()->attach($usersUpdate['role']);

        return redirect()->back()->with('success', 'Successfully Edited User!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id)->delete();
		 return \Response::json( $user );
	}

	public function password( $id ) {

		$user = User::find( $id );
		return view( 'admin.changepassword', compact( 'user' ) );
	}
	/**
	 * Custom Change Password.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function changepassword( $id ) {


		$rules = [
            'password' 	=> 'required|confirmed|min:6',
        ];

        $input = Input::only(
            'password',
            'password_confirmation'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

		$user = User::find( $id );
		$user->password = \Hash::make( Input::get('password') );
		$user->save();

		return redirect()->back()->with('success', 'Successfully Changed Password!');
	}

	public function documents() {
		$application = Application::all();
		$documents   = Documents::with('application')->orderBy('created_at','DESC')->get();
		return view('admin.documents', compact('application', 'documents'));
	}

	public function storedocument( Request $request ) {


		$docs = new Documents();
		$docs->title 		  = $request->input('title');
		$docs->description 	  = $request->input('description');
		$docs->applications_id = $request->input('application_id');
		$docs->save();

		$customer = Customers::where('applications_id', '=', $request->input('application_id') )->get();

		if( count( $customer ) > 0 ) {
			foreach ($customer as $key => $value) {
				
				$cust_doc = new CustomersRequiredDocuments();
				$cust_doc->customers_id = $value->id;
				$cust_doc->documents_id = $docs->id;
				$cust_doc->applications_id = $request->input('application_id');
				$cust_doc->save();

			}
		}
		return \Response::json( $docs );
	}
}
