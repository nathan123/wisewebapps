<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CustomersSpouse;
use Illuminate\Http\Request;
use App\Customers;
use Auth;
use App\CustomersActivityLog;

class CustomersSpouseController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{	
		$customers_spouse = CustomersSpouse::find( $id );
		return view('admin.editspouse', compact('customers_spouse'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$customersspouseUpdate = \Request::all();
		$customers_spouse      = CustomersSpouse::find($id);		
		$customers_spouse->update($customersspouseUpdate);

		$cust_rec = Customers::find($customers_spouse->customers_id);

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $cust_rec->user_id;
		$customersactivitylog->customers_id = $cust_rec->id;
		$customersactivitylog->activity     = 'Successfully Updated Customers Spouse';
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();


		return redirect()->back()->with('success','Successfully Edited Customer Spouse.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
