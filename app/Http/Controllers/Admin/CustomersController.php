<?php 

namespace App\Http\Controllers\Admin;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Customers;
use App\CustomersFile;
use App\CustomersPayment;
use App\CustomersActivityLog;
use App\CustomersAdditionalInfo;
use App\CustomersFamilyComposition;
use App\CustomersEducation;
use App\CustomersWorkHistory;
use App\CustomersLanguage;
use App\CustomersSpouse;
use App\CustomersSpouseEducation;
use App\CustomersSpouseWorkHistory;
use App\CustomersSpouseLanguage;
use App\CustomersApplication;
use App\Documents;
use App\CustomersRequiredDocuments;
use App\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class CustomersController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		if( Auth::user()->hasRole('admin') || Auth::user()->hasRole('receptionist') || Auth::user()->hasRole('encoder')  || Auth::user()->hasRole('accounting') || Auth::user()->hasRole('document') ) {

			$customers = Customers::with('user')->orderBy('created_at','DESC')->get();
			

		}
		else {

			$filters    = Auth::user()->id;
			$customers  = Customers::with('user')->orderBy('created_at','DESC')->get();
			#$customers = Customers::with('user')->where( 'user_id', '=', Auth::user()->id )->orderBy('created_at','DESC')->get();			
			
		}

		return view('admin.customers', compact( 'customers', 'filters' ) );
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	
		if( !Auth::user()->hasRole('case_officer') ) {
			$users 		 = User::with('roles')->orderBy('customer_count', 'ASC')->get();
			$application = Application::all();

			return view('admin.addcustomers', compact('users','application'));
		}

		return view('errors.404');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{	

		$rules = [
            'email' 	=> 'required|email|unique:customers',
        ];

        $input = Input::only(
            'email'
        );

        $validator = Validator::make($input, $rules);
        if($validator->fails())
        {
            return \Response::json( 'email' );
        }

        $firstname = strtolower( $request->input('firstname') );
        $lastname  = strtolower( $request->input('lastname') );

		$customers = new Customers();

		 if( $customers->checkname( $firstname, $lastname ) ) {
        	 return \Response::json( 'name' );
        }

		$customers->applications_id = !empty( $request->input('applications_id') ) ? $request->input('applications_id') : null;
		$customers->user_id 		= $request->input('user_id');
		$customers->source 			= $request->input('source');
		$customers->firstname 		= $firstname;
		$customers->lastname 		= $lastname;
		$customers->middle_name 	= !empty( $request->input('middle_name') ) ? $request->input('middle_name') : null;
		$customers->date_of_birth 	= $request->input('date_of_birth');
		$customers->gender 			= !empty( $request->input('gender') ) ? $request->input('gender') : null;
		$customers->contact_number 	= $request->input('contact_number');
		$customers->email 			= $request->input('email');
		$customers->city 		 	= $request->input('city');
		$customers->province 		= $request->input('province');
		$customers->country 		= $request->input('country');
		$customers->profession 		= $request->input('profession');
		$customers->secondary_email = !empty( $request->input('secondary_email') ) ? $request->input('secondary_email') : null;
		$customers->other_email 	= !empty( $request->input('other_email') ) ? $request->input('other_email') : null;
		$customers->secondary_contact_number  = !empty( $request->input('secondary_contact_number') ) ? $request->input('secondary_contact_number') : null;
		$customers->other_contact_number 	  = !empty( $request->input('other_contact_number') ) ? $request->input('other_contact_number') : null;
		$customers->meta_description_first    = !empty( $request->input('meta_description_first') ) ? $request->input('meta_description_first') : null;
		$customers->meta_description_second   = !empty( $request->input('meta_description_second') ) ? $request->input('meta_description_second') : null;
		
		$customers->save();

		$user = User::find( $request->input('user_id') );
		$user->customer_count = $user->customer_count + 1;
		$user->save();

		if( !empty( $request->input('file') ) ){

			$file = CustomersFile::find($request->input('file'));
			// $file->name 		= $customers->name . '_' . $customers->id . '.' . $file->extension;
			$file->customers_id = $customers->id;
			$file->save();
		}


		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $request->input('user_id');
		$customersactivitylog->customers_id = $customers->id;
		$customersactivitylog->activity     = 'Add Customer';
		$customersactivitylog->editor       =  Auth::user()->name;
		$customersactivitylog->save();


		return \Response::json( $customers );

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{	

			$customers        = Customers::with('family','education','work','language','file','info','documents')->find($id);

			if( count( $customers ) > 0 ) {

				if ( !Auth::user()->hasRole('receptionist') && !Auth::user()->hasRole('encoder') ) {

					$customers_spouse = CustomersSpouse::with('education','work','language')
										->where( 'customers_id','=',$customers->id )
										->get();

					if( !is_null( $customers->applications_id ) ) {
						$application 	  = Application::whereId( $customers->applications_id )->get();
					}

					$case_officer 	  = User::where('id', '=', $customers->user_id )->get();
					$customers_file   = CustomersFile::where( 'customers_id','=',$customers->id )->get();				
					$payment     	  = CustomersPayment::with('user')->where('customers_id', '=', $id )->get();     
					$customers_activity_log = CustomersActivityLog::with('user')->where('customers_id','=',$customers->id)->orderBy('created_at', 'DESC')->get();

					if( !Auth::user()->hasRole('admin') ) {
						$customers->is_read = 1;
						// $customers->status  = 'ongoing';
						$customers->save();
					}

					return view( 'admin.viewcustomer', 
							compact( 'customers', 'case_officer', 'customers_spouse', 'customers_file','customers_activity_log','payment','application' 
					) );

				} else {

					return view('errors.404');
				}

			} 

			else 
				return view('errors.404');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$customers = Customers::find( $id );

		if( !Auth::user()->hasRole('receptionist') && !Auth::user()->hasRole('encoder') ){
			$users = User::with('roles')
						->orderBy('customer_count', 'ASC')
						->get();

			$customers_file = CustomersFile::where( 'customers_id','=',$customers->id )->get();
			$application 	= Application::all();

			return view('admin.editcustomers', compact( 'customers', 'users', 'customers_file','application' ));
		} 
		return view('errors.404');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{	

		$customersUpdate = \Request::all();
		// print_r( $customersUpdate );
		// die();
		$customers   	 = Customers::find($id);		
		// $customers->update($customersUpdate);

		$customers->applications_id = !empty( $customersUpdate['applications_id'] ) ? $customersUpdate['applications_id'] : null;

		$customers->user_id 		= $customersUpdate['user_id'];
		$customers->source 			= $customersUpdate['source'];
		$customers->firstname 		= $customersUpdate['firstname'];
		$customers->lastname 		= $customersUpdate['lastname'];
		$customers->middle_name 	= !empty( $customersUpdate['middle_name'] ) ? $customersUpdate['middle_name'] : null;
		$customers->date_of_birth 	= $customersUpdate['date_of_birth'];
		$customers->gender 			= !empty( $customersUpdate['gender'] ) ? $customersUpdate['gender'] : null;
		$customers->contact_number 	= $customersUpdate['contact_number'];
		$customers->email 			= $customersUpdate['email'];
		$customers->city 		 	= $customersUpdate['city'];
		$customers->province 		= $customersUpdate['province'];
		$customers->country 		= $customersUpdate['country'];
		$customers->profession 		= $customersUpdate['profession'];
		$customers->secondary_email = !empty( $customersUpdate['secondary_email'] ) ? $customersUpdate['secondary_email'] : null;
		$customers->other_email 	= !empty( $customersUpdate['other_email'] ) ? $customersUpdate['other_email'] : null;
		$customers->secondary_contact_number  = !empty( $customersUpdate['secondary_contact_number'] ) ? $customersUpdate['secondary_contact_number'] : null;
		$customers->other_contact_number 	  = !empty( $customersUpdate['other_contact_number'] ) ? $customersUpdate['other_contact_number'] : null;
		$customers->meta_description_first    = !empty( $customersUpdate['meta_description_first'] ) ? $customersUpdate['meta_description_first'] : null;
		$customers->meta_description_second   = !empty( $customersUpdate['meta_description_second'] ) ? $customersUpdate['meta_description_second'] : null;

		$customers->save();

		// $user = User::find( $customers->user_id );
		// $user->customer_count = $user->customer_count + 1;
		// $user->save();

		if( !empty( $customersUpdate['file_id'] ) ){

			$file = CustomersFile::find($customersUpdate['file_id']);
			$path = public_path()."/uploads\\";

			// $file->name 		= $customers->firstname . '_' . $customers->lastname . '_' . $customers->id . '.' . $file['extension'];
			$file->customers_id = $customers->id;
			$file->save();

			
		}

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $customersUpdate['user_id'];
		$customersactivitylog->customers_id = $id;
		$customersactivitylog->activity     = 'Edit Customer Information';
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();

		if ( !is_null( $customers->applications_id ) ) {
			$docs = Documents::where('applications_id', '=', $customers->applications_id )->get();
			foreach ($docs as $key => $value) {
				$add = new CustomersRequiredDocuments();
				$add->customers_id    = $customers->id;
				$add->documents_id 	  = $value->id;
				$add->applications_id = $customers->applications_id;
				$add->save();
			}
		}
		

		return redirect('admin/customers/'.$id)->with('success','Successfully Edited Customer.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		$cust = Customers::find($id);
		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $cust->user_id;
		$customersactivitylog->customers_id = $id;
		$customersactivitylog->activity     = 'Deleted Customer ' . ucfirst( $cust->firstname ) . ' ' . ucfirst( $cust->lastname );
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();

		$customers = Customers::find($id)->delete();
		 return \Response::json( true );
	}

	/**
	 * Approve Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function approve( $id ) {

		$customers = Customers::find($id);
		$customers->status = 'approve';
		$customers->save();

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $customers->user_id;
		$customersactivitylog->customers_id = $id;
		$customersactivitylog->activity     = 'Customer Status Approved';
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();

		return \Response::json( $customers );
	}

		/**
	 * Disapprove Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function disapprove( $id ) {

		$customers = Customers::find($id);
		$customers->status = 'disapprove';
		$customers->save();

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $customers->user_id;
		$customersactivitylog->customers_id = $id;
		$customersactivitylog->activity     = 'Customer Status Disapproved';
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();

		return \Response::json( $customers );
	}

		/**
	 * Hold Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function hold( $id ) {

		$customers = Customers::find($id);
		$customers->status = 'hold';
		$customers->save();

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $customers->user_id;
		$customersactivitylog->customers_id = $id;
		$customersactivitylog->activity     = 'Customer Status Hold';
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();

		return \Response::json( $customers );
	}

		/**
	 * Unhold Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function unhold( $id ) {

		$customers = Customers::find($id);
		$customers->status = 'unhold';
		$customers->save();

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $customers->user_id;
		$customersactivitylog->customers_id = $id;
		$customersactivitylog->activity     = 'Customer Status Unhold';
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();

		return \Response::json( $customers );
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteresume( $id )
	{
		$resume = CustomersFile::find($id)->delete();
		 return \Response::json( $resume );
	}

	
	/**
	 * Upload File
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function upload()
	{
		if ( Input::file('file')->isValid() ) {
			
			$destinationPath = public_path()."/uploads\\";
			$extension       = Input::file('file')->getClientOriginalExtension();
			$fileName        = rand(11111,99999).'.'.$extension;
			$file 			 = Input::file('file')->move($destinationPath, $fileName);

			$data = array(
				'realpath'  => $destinationPath,
				'origname'  => $fileName,
				'extension' => Input::file('file')->getClientOriginalExtension(),
				'size'		=> filesize( $file )
				// 'mimetype'  => \File::mimeType( $file )
			);

			$resume = new CustomersFile();
			$resume->name 		= $data['origname'];
			$resume->path 	    = $data['realpath'];
			$resume->extension  = $data['extension'];
			$resume->size 		= $data['size'];
			// $pagemeta->mime = $data['mimetype'];
			$resume->save();

			return \Response::json( $resume );
		}

		else {
	     	// sending back with error message.
	      	return 'error';
	     	 
	    }
	}

	/**
	 * Add Family Composition To Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function addfamily($id) {

		$customers = $id;
		return view('admin.addfamily', compact('customers'));

	}

	/**
	 * Add Education To Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function addeducation($id) {

		$customers = $id;
		return view('admin.addeducation', compact('customers'));

	}

	/**
	 * Add Work To Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function addwork($id) {

		$customers = $id;
		return view('admin.addwork', compact('customers'));

	}

	/**
	 * Add Language To Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function addlanguage($id) {

		$customers = $id;
		return view('admin.addlanguage', compact('customers'));

	}

	/**
	 * Store Family Composition To Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function storefamily($customers_id) {

		$customers = $customers_id;
		$input     = \Request::all();
		$custfamilycomposition = new CustomersFamilyComposition();

		$custfamilycomposition->customers_id = $customers;
		$custfamilycomposition->status 	     = $input['status'];
		$custfamilycomposition->number_of_children 	 = $input['number_of_children'];
		$custfamilycomposition->save();

		$this->add_activity_log( $customers_id, 'Successfully Added Family Composition');

		return \Response::json( $custfamilycomposition );
	}

	/**
	 * Store Education To Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function storeeducation() {

		$input = \Request::all();

		$data = array();
		parse_str( $input['data'], $data );

		// print_r( $data );
		// die();
		

		for( $i = 1; $i <= $input['count']; $i++ ){

			$custeducation = new CustomersEducation();

			$custeducation->customers_id = $data['customers_id'];
			$custeducation->status = $data['status'.$i];
			$custeducation->from = $data['from'.$i];
			$custeducation->to = $data['to'.$i];
			$custeducation->name_of_school = $data['name_of_school'.$i];
			$custeducation->qualification = $data['qualification'.$i];
			$custeducation->save();
			
		}

		$this->add_activity_log( $data['customers_id'], 'Successfully Added Education Info');

		return \Response::json( $custeducation );
	}

	/**
	 * Store Work To Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function storework() {

		$input = \Request::all();
		$data = array();
		parse_str( $input['data'], $data );

		for( $i = 1; $i <= $input['count']; $i++ ){

			$custwork = new CustomersWorkHistory();

			$custwork->customers_id = $data['customers_id'];
			$custwork->from = $data['from'.$i];
			$custwork->to = $data['to'.$i];
			$custwork->name_of_company = $data['name_of_company'.$i];
			$custwork->designation = $data['designation'.$i];
			$custwork->save();
			
		}

		$this->add_activity_log(  $data['customers_id'], 'Successfully Added Work History');

		return \Response::json( $custwork );
	}

	/**
	 * Store Language To Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function storelanguage() {

		$input = \Request::all();
		$data = array();
		parse_str( $input['data'], $data );

		for( $i = 1; $i <= $input['count']; $i++ ){

			$custlang = new CustomersLanguage();

			$custlang->customers_id = $data['customers_id'];
			$custlang->language  = $data['language'.$i];
			$custlang->speaking  = $data['speaking'.$i];
			$custlang->listening = $data['listening'.$i];
			$custlang->reading   = $data['reading'.$i];
			$custlang->writing   = $data['writing'.$i];
			$custlang->save();
			
		}

		$this->add_activity_log(  $data['customers_id'], 'Successfully Added Language');

		return \Response::json( $custlang );

	}

	/**
	 * Add Spouse To Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function addspouse($id) {

		$customers = $id;
		return view('admin.addspouse', compact('customers'));

	}


	/**
	 * Add Spouse Education
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function addspouseeducation($id, $customers_id) {

		$spouse = $id;
		$customer = $customers_id;

		return view('admin.addspouseeducation', compact('spouse', 'customer'));

	}

	/**
	 * Add Spouse Education
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function addspousework($id, $customers_id) {

		$spouse = $id;
		$customer = $customers_id;
		return view('admin.addspousework', compact('spouse','customer'));

	}

	/**
	 * Add Spouse Language
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function addspouselanguage($id, $customers_id) {

		$spouse = $id;
		$customer = $customers_id;
		return view('admin.addspouselanguage', compact('spouse', 'customer'));

	}


	/**
	 * Store Spouse
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function storespouse( Request $request ) {

		$rules = [
            'email' 	=> 'required|email|unique:customers_spouses'
        ];

        $input = Input::only(
            'email'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return \Response::json( $validator->fails() );
        }

		$customers_spouse = new CustomersSpouse();

		$customers_spouse->customers_id		= $request->input('customers_id');
		$customers_spouse->name 			= $request->input('name');
		$customers_spouse->date_of_birth 	= $request->input('date_of_birth');
		$customers_spouse->contact_number 	= $request->input('contact_number');
		$customers_spouse->email 			= $request->input('email');

		$customers_spouse->save();

		$this->add_activity_log(  $request->input('customers_id'), 'Successfully Added Spouse.');

		return \Response::json( $customers_spouse );

	}	

	/**
	 * Store Spouse Education To Customer
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function storespouseeducation() {

		$input = \Request::all();

		$data = array();
		parse_str( $input['data'], $data );

		// print_r( $data );
		// die();
		

		for( $i = 1; $i <= $input['count']; $i++ ){

			$custspouseeducation = new CustomersSpouseEducation();

			$custspouseeducation->customers_spouse_id  = $data['customers_spouse_id'];
			$custspouseeducation->status 		= $data['status'.$i];
			$custspouseeducation->from 		    = $data['from'.$i];
			$custspouseeducation->to 			= $data['to'.$i];
			$custspouseeducation->name_of_school = $data['name_of_school'.$i];
			$custspouseeducation->qualification = $data['qualification'.$i];
			$custspouseeducation->save();
			
		}

		$spouse_id = CustomersSpouse::find( $data['customers_spouse_id'] );
		$this->add_activity_log( $spouse_id->customers_id, 'Successfully Added Spouse Education.');

		return \Response::json( $custspouseeducation );
	}

	/**
	 * Store Work To Spouse
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function storespousework() {

		$input = \Request::all();
		$data = array();
		parse_str( $input['data'], $data );

		for( $i = 1; $i <= $input['count']; $i++ ){

			$custspousework = new CustomersSpouseWorkHistory();

			$custspousework->customers_spouse_id = $data['customers_spouse_id'];
			$custspousework->from = $data['from'.$i];
			$custspousework->to = $data['to'.$i];
			$custspousework->name_of_company = $data['name_of_company'.$i];
			$custspousework->designation = $data['designation'.$i];
			$custspousework->save();
			
		}

		$spouse_id = CustomersSpouse::find( $data['customers_spouse_id'] );
		$this->add_activity_log( $spouse_id->customers_id, 'Successfully Added Spouse Work History');

		return \Response::json( $custspousework );
	}


	/**
	 * Store Language To Spouse
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function storespouselanguage() {

		$input = \Request::all();
		$data = array();
		parse_str( $input['data'], $data );

		for( $i = 1; $i <= $input['count']; $i++ ){

			$custspouselang = new CustomersSpouseLanguage();

			$custspouselang->customers_spouse_id = $data['customers_spouse_id'];
			$custspouselang->language  = $data['language'.$i];
			$custspouselang->speaking  = $data['speaking'.$i];
			$custspouselang->listening = $data['listening'.$i];
			$custspouselang->reading   = $data['reading'.$i];
			$custspouselang->writing   = $data['writing'.$i];
			$custspouselang->save();
			
		}

		$spouse_id = CustomersSpouse::find( $data['customers_spouse_id'] );
		$this->add_activity_log( $spouse_id->customers_id, 'Successfully Added Spouse Landguage');

		return \Response::json( $custspouselang );

	}


	/**
	 * Search Customers Lastname
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function searchname($string) {

		if(  !empty( $string ) ) {
			$customer = Customers::where( 'lastname', 'LIKE', '%'.$string.'%' )
						->orderBy('created_at', 'DESC')
						->get();

			return \Response::json( $customer );
		}

		return false;
	}

	public function addpayment(Request $request) {

		$addpayment = new CustomersPayment();
		$addpayment->title  		= $request->input('type');
		$addpayment->amount 		= $request->input('amount');
		$addpayment->customers_id   = $request->input('customers_id');
		$addpayment->user_id        = Auth::user()->id;
		$addpayment->save();

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $request->input('user_id');
		$customersactivitylog->customers_id = $request->input('customers_id');
		$customersactivitylog->activity     = $request->input('type');
		$customersactivitylog->editor  		= Auth::user()->name;
		$customersactivitylog->save();


		return \Response::json( $addpayment );
	}

	public function hasdocument( Request $request ) {

		$customers_docs = DB::table('customers_documents')
            ->where('customers_id', '=', $request->input('customers_id') )
            ->where('documents_id', '=', $request->input('documents_id') )
            ->update(['is_available' => 1]);

        $customers = Customers::find( $request->input('customers_id') );
        $docs      = Documents::find( $request->input('documents_id') );

        $customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $customers->user_id;
		$customersactivitylog->customers_id = $request->input('customers_id');
		$customersactivitylog->activity     = ucwords( $customers->firstname . ' ' . $customers->lastname ) . ' Submitted ' . ucwords( $docs->title );
		$customersactivitylog->editor  		= Auth::user()->name;
		$customersactivitylog->save();

		return \Response::json( 1 );
	}

	public function removedocument( Request $request ) {

		$customers_docs = DB::table('customers_documents')
            ->where('customers_id', '=', $request->input('customers_id') )
            ->where('documents_id', '=', $request->input('documents_id') )
            ->update(['is_available' => 0]);

        $customers = Customers::find( $request->input('customers_id') );
        $docs      = Documents::find( $request->input('documents_id') );

        $customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $customers->user_id;
		$customersactivitylog->customers_id = $request->input('customers_id');
		$customersactivitylog->activity     = ucwords( $customers->firstname . ' ' . $customers->lastname ) . ' Removed ' . ucwords( $docs->title );
		$customersactivitylog->editor  		= Auth::user()->name;
		$customersactivitylog->save();

		return \Response::json( 0 );
	}

	public function add_activity_log( $id, $desc = null ) {

		$cust_rec = Customers::find( $id );

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $cust_rec->user_id;
		$customersactivitylog->customers_id = $cust_rec->id;
		$customersactivitylog->activity     = $desc;
		$customersactivitylog->editor       = Auth::user()->name;
		$customersactivitylog->save();

		return true;
	}
}
