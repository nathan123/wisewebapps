<?php namespace App\Http\Controllers\Admin;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CustomersActivityLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.dashboard');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Show Activity logs.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function activity()
	{	


		$activity =  CustomersActivityLog::with('user')
					->orderBy( 'customers_activity_logs.created_at', 'DESC' )
					->get();
	
		return view('admin.activity', compact('activity'));
	}

	/**
	 * Set Logs To Read.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function activitylog() {

		$query = DB::table('customers_activity_logs')->update(['is_read' => true ]);
		return \Response::json( $query );
	}


	/**
	 * Store Activity logs.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function storeActivity(Request $request) {

		$customersactivitylog = new CustomersActivityLog();
		$customersactivitylog->user_id      = $request->input('user_id');
		$customersactivitylog->customers_id = $request->input('customers_id');
		$customersactivitylog->activity     = $request->input('activity');
		$customersactivitylog->editor  		= Auth::user()->name;
		$customersactivitylog->save();

		return \Response::json( $customersactivitylog );
	}

	public function countactivity() {
		return CustomersActivityLog::count();
	}
}
