<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersFamilyComposition extends Model {

	protected $table = 'customers_family_compositions';

	protected $fillable = [
			'customers_id',
			'status',
			'number_of_children',
	];

	public function customers() {
		return $this->belongsTo('App\Customers');
	}

}
