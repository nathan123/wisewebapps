<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersSpouseWorkHistory extends Model {

	protected $table = 'customers_spouse_work_histories';
	
	protected $fillable = [
			'spouses_id',
			'from',
			'to',
			'name_of_company',
			'designation'
	];

	public function customers() {
		return $this->belongsTo('App\CustomersSpouse');
	}

}
