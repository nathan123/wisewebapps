<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersEducation extends Model {

	protected $table = 'customers_educations';
	
	protected $fillable = [
			'customers_id',
			'status',
			'from',
			'to',
			'name_of_school',
			'qualification'
	];

	public function customers() {
		return $this->belongsTo('App\Customers');
	}

}
