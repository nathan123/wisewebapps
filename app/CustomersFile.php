<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersFile extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	protected $table = 'customers_files';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	
	protected $fillable = [
				'customers_id', 
				'name', 
				'path', 
				'extension', 
				'size', 
				'mime'
			];


	public function customers() {
		return $this->belongsTo('App\Customers');
	}

}
