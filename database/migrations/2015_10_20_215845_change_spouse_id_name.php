<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSpouseIdName extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */

	public function up()
	{
		
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::table('customers_spouse_educations', function(Blueprint $table)
		{
			$table->dropForeign(['spouses_id']);
			$table->dropColumn('spouses_id');
			
		});

		Schema::table('customers_spouse_languages', function(Blueprint $table)
		{
			$table->dropForeign(['spouses_id']);
			$table->dropColumn('spouses_id');
		});

		Schema::table('customers_spouse_work_histories', function(Blueprint $table)
		{
			$table->dropForeign(['spouses_id']);
			$table->dropColumn('spouses_id');
			
		});

		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		
		Schema::table('customers_spouse_educations', function(Blueprint $table)
		{
			$table->integer('spouses_id')->unsigned();
			$table->foreign('spouses_id')->references('id')->on('customers_spouses')
                  ->onUpdate('cascade')->onDelete('cascade');
			
		});

		Schema::table('customers_spouse_languages', function(Blueprint $table)
		{
			$table->integer('spouses_id')->unsigned();
			$table->foreign('spouses_id')->references('id')->on('customers_spouses')
                  ->onUpdate('cascade')->onDelete('cascade');
			
		});

		Schema::table('customers_spouse_work_histories', function(Blueprint $table)
		{
			$table->integer('spouses_id')->unsigned();
			$table->foreign('spouses_id')->references('id')->on('customers_spouses')
                  ->onUpdate('cascade')->onDelete('cascade');			
		});

		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
