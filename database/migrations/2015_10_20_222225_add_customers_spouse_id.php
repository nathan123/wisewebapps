<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomersSpouseId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('customers_spouse_educations', function(Blueprint $table)
		{
			$table->integer('customers_spouse_id')->unsigned();
			$table->foreign('customers_spouse_id')->references('id')->on('customers_spouses')
                  ->onUpdate('cascade')->onDelete('cascade');
			
		});

		Schema::table('customers_spouse_languages', function(Blueprint $table)
		{
			$table->integer('customers_spouse_id')->unsigned();
			$table->foreign('customers_spouse_id')->references('id')->on('customers_spouses')
                  ->onUpdate('cascade')->onDelete('cascade');
			
		});

		Schema::table('customers_spouse_work_histories', function(Blueprint $table)
		{
			$table->integer('customers_spouse_id')->unsigned();
			$table->foreign('customers_spouse_id')->references('id')->on('customers_spouses')
                  ->onUpdate('cascade')->onDelete('cascade');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::table('customers_spouse_educations', function(Blueprint $table)
		{
			$table->dropForeign(['customers_spouse_id']);
			$table->dropColumn('customers_spouse_id');
			
		});

		Schema::table('customers_spouse_languages', function(Blueprint $table)
		{
			$table->dropForeign(['customers_spouse_id']);
			$table->dropColumn('customers_spouse_id');
		});

		Schema::table('customers_spouse_work_histories', function(Blueprint $table)
		{
			$table->dropForeign(['customers_spouse_id']);
			$table->dropColumn('customers_spouse_id');
			
		});

		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
