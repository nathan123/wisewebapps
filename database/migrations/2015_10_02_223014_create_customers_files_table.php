<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers_files', function(Blueprint $table)
		{
			$table->increments('id');		
			$table->integer('customers_id')->nullable();
			$table->string('name')->nullable();
			$table->string('path')->nullable();
			$table->string('extension')->nullable();
			$table->integer('size')->nullable();
			$table->string('mime')->nullable();


			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers_files');
	}

}
