<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersSpousesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers_spouses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customers_id')->unsigned();
			$table->string('name');
			$table->date('date_of_birth');
			$table->integer('contact_number')->nullable();
			$table->string('email')->unique();

			$table->foreign('customers_id')->references('id')->on('customers')
                  ->onUpdate('cascade')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers_spouses');
	}

}
