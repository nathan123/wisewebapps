<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersAdditionalInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers_additional_infos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customers_id')->unsigned();
			$table->string('key')->nullable();
			$table->string('value')->nullable();

			$table->foreign('customers_id')->references('id')->on('customers')
                  ->onUpdate('cascade')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers_additional_infos');
	}

}
