<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersSpouseEducationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers_spouse_educations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('spouses_id')->unsigned();
			$table->enum('status',['graduate','undergraduate','postgrad','ongoing']);
			$table->date('from')->nullable();
			$table->date('to')->nullable();
			$table->string('name_of_school')->nullable();
			$table->string('qualification')->nullable();

			$table->foreign('spouses_id')->references('id')->on('customers_spouses')
                  ->onUpdate('cascade')->onDelete('cascade');
                  
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers_spouse_educations');
	}

}
