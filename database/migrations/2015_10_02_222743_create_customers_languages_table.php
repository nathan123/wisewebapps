<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers_languages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customers_id')->unsigned();
			$table->string('language');
			$table->integer('speaking')->default(0);
			$table->integer('listening')->default(0);
			$table->integer('reading')->default(0);
			$table->integer('writing')->default(0);

			$table->foreign('customers_id')->references('id')->on('customers')
                  ->onUpdate('cascade')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers_languages');
	}

}
