<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersSpouseLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers_spouse_languages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('spouses_id')->unsigned();
			$table->string('language');
			$table->integer('speaking')->default(0);
			$table->integer('listening')->default(0);
			$table->integer('reading')->default(0);
			$table->integer('writing')->default(0);
			
			$table->foreign('spouses_id')->references('id')->on('customers_spouses')
                  ->onUpdate('cascade')->onDelete('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers_spouse_languages');
	}

}
