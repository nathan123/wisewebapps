<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersWorkHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers_work_histories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customers_id')->unsigned();
			$table->date('from')->nullable();
			$table->date('to')->nullable();
			$table->string('name_of_company')->nullable();
			$table->string('designation')->nullable();

			$table->foreign('customers_id')->references('id')->on('customers')
                  ->onUpdate('cascade')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers_work_histories');
	}

}
