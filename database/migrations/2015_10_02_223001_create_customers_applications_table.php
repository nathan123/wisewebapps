<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers_applications', function(Blueprint $table)
		{
			$table->integer('customers_id')->unsigned();
			$table->foreign('customers_id')->references('id')->on('customers')
                  ->onUpdate('cascade')->onDelete('cascade');

            $table->integer('applications_id')->unsigned();
			$table->foreign('applications_id')->references('id')->on('applications')
                  ->onUpdate('cascade')->onDelete('cascade');  

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers_applications');
	}

}
