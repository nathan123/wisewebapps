@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			
			
			<div class="form-wrapper">
				<div class="logo-wrapper">
						<h1></h1>
				</div>

				<div class="form-panel-box clearfix">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
			
					<form class="form-horizontal" id="form-login" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<div class="col-md-12">
								<div class="inner-addon left-addon">
						            <i class="glyphicon glyphicon-user"></i>
						            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" />
					        	</div>
					        </div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<div class="inner-addon left-addon">
						            <i class="glyphicon glyphicon-lock"></i>
						            <input type="password" name="password" class="form-control" placeholder="Password" />
					        	</div>
					        </div>
						</div>						

						<div class="form-group">
							<div class="col-md-6">
								<button type="submit" class="btn btn-large btn-primary" style="width:50%">Login</button>

								<!-- <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a> -->
							</div>
						</div>

						<!-- <div class="form-group">
							<div class="col-md-6 col-sm-offset-2">								
								<label>
									<input type="checkbox" name="remember"> <a href="{!! url('/auth/register') !!}">Register Employee</a>
								</label>							
							</div>
						</div> -->

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
