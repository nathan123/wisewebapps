@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">EDIT CUSTOMER SPOUSE</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
              <a href="{!! URL::to('admin/customers/'.$cust_id) !!}">Back</a>
        @if( session('success') )
           		 	<div class="clearfix"><span class="alert alert-success" style="display: inherit;">{!! session('success') !!}</span></div>
				@endif
				<h2>Edit Customers Spouse Education Information</h2>
    				<div class="customers-info clearfix">
    					<div class="col-md-12">
    						{!! Form::model( $spouse_education,['method' => 'PATCH','route'=>['admin.customers.spouse.education.update',$spouse_education->id],  'id'=>'editcustomersspouseeducation' ]) !!}

    		                	<div class="form-group">
    		                	   {!! Form::label('status', 'Status:') !!}
          						   <select name="status" class="form-control">
          						   			
          						   		<option selected>
          						   			@if( $spouse_education->status == 'graduate' )
          						   				Graduate
          						   			@elseif( $spouse_education->status == 'undergraduate' )
          						   				Undergraduate
          						   			@elseif( $spouse_education->status == 'postgrad' )
          						   				Post Graduate
          						   			@else
          						   				On Going
          						   			@endif
          						   		</option>

          						   		<option value="graduate">Graduate</option>
          						   		<option value="undergraduate">UnderGraduate</option>
          						   		<option value="ongoing">On Going</option>
          						   		<option value="postgrad">Post Graduate</option>
          						   		
          						   </select>
    			                </div>

    			                <div class="form-group">
    			                   {!! Form::label('name_of_school', 'Name Of School :') !!}
          						   {!! Form::text('name_of_school',null,['class'=>'form-control']) !!}
    			                </div>
    			               		   
    			                <div class="form-group">
                                    <input type="date" name="from" class="form-control" value="{!! $spouse_education->from !!}" />
                                </div>

                                <div class="form-group">
                                    <input type="date" name="to" class="form-control" value="{!! $spouse_education->to !!}" />
                                </div>

                                <div class="form-group">
                                  {!! Form::text('qualification',null,['class'=>'form-control required']) !!}
                                </div>

    						    <div class="form-group">
    						        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    						    </div>
    							
    					    {!! Form::close() !!}
    					</div>
    				</div>

                
            </div>
        </div>
    </div>
@stop