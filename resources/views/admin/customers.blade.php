@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">CUSTOMERS</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
            <span class="customer-deleted"></span>
               <div class="panel panel-default">
                  <div class="panel-heading clearfix">
                    <div class='col-md-6'>
                      @foreach( $role as $k => $v )
                        @if( $v->name == 'receptionist' || $v->name == 'admin' || $v->name == 'encoder' )
                          <a href="{!! url('admin/customers/create') !!}" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> &nbsp;Add Customer</a>
                        @endif
                      @endforeach
                    </div>

                    <div class='col-md-6 clearfix'>
                      
                      <ul class="list list-inline pull-right btn-actions">
                        <li><button id="print" class="btn btn-primary">Print</button></li>
                        <li> <a href="{!! url('admin/customers/exportexcel') !!}" class="btn btn-primary" id="exportexcel" >Export to Excel</a></li>
                        <li> <a href="{!! url('admin/customers/exportpdf') !!}" class="btn btn-primary" id="exportpdf" >Export to PDF</a></li>
                      </ul>

                    </div>

                    <div class="deletecustmessage clearfix" style="width:100%"></div>
                    
                  </div>
                  <div id="customer">
                   <table id="customers" class="table table-striped table-bordered table-list-page" data-page-length='5'>
                      <thead>
                        <tr>
                          <th width="15%">Name</th>
                          <th width="10%">Email</th>
                          <th>Gender</th>
                          <th>Contact Number</th>  
                          <th width="15%">Case Officer</th> 
                          <th width="13%">Status</th>
                          @foreach( $role as $r => $h )
                            @if( $h->name == 'case_officer' || $h->name == 'admin' || $h->name == 'accounting' || $h->name == 'document'  )
                              <th class="action">Action</th>
                            @endif
                          @endforeach  
                        </tr>
                      </thead>
                      <tbody>
                          @foreach( $customers as $key => $value )

                            @if( $value->is_read == 0  )
                              <tr style="font-weight: bold; color: #12A5F4">
                            @else 
                              <tr>
                            @endif
                                <td>{!!  ucwords( $value->lastname ) . ', '.  ucwords( $value->firstname ). ' ' . ucwords( $value->middle_name ) !!}</td>
                                <td>{!! $value->email !!}</td>
                                <td>
                                    @if( $value->gender == 'M' )
                                       MALE
                                    @else
                                      FEMALE
                                    @endif
                                </td>
                                <td>{!! $value->contact_number !!}</td>
                                <td>
                                    {!! ucwords( $value->user->name ) !!}
                                </td>
                                <td>{!! $value->status !!}</td>
                                @foreach( $role as $a => $b )
                                  @if( $b->name == 'case_officer' || $b->name == 'admin' || $b->name == 'accounting' || $b->name == 'document' )
                                    <td class="action">         

                                     <ul class="list list-inline text-center">
                                       <li> <a href="{!! url('admin/customers/'. $value->id ); !!}" class="read"><i class="glyphicon glyphicon-pencil"></i></a></li>
                                       <li>                                         
                                          @if( $b->name == 'admin' )                                         
                                            <a href="#" data-id="{!! $value->id !!}" data-url="{!! url('admin/customers/'.$value->id) !!}" data-token="{!! csrf_token() !!}" class="custdelete"><i class="glyphicon glyphicon-trash "></i></a> 
                                          @endif
                                       </li>
                                     </ul>
                                    
                                    </td>
                                  @endif
                                @endforeach                                
                              </tr> 

                          @endforeach
                      </tbody>
                   </table>
                  </div>
                  @if( !empty( $filters ) )
                    <input type="hidden" id="filters" value="{!! $filters !!}" />
                     @foreach( $customers as $key => $value )
                        <input type="hidden" id="filter_name" value="{!! ucwords( Auth::user()->name ) !!}" />
                     @endforeach
                  @endif
                </div>
            </div>
        </div>
    </div>
<div id="deletecustomer" class="modal fade" role="dialog" >
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-body">Succesfully Deleted Customer.</div>
  </div>
  </div>
</div>
@stop