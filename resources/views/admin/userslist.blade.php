@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">Users</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
               <span class="user-deleted"></span>
               <div class="panel panel-default">
                  <div class="panel-heading"><a href="{!! url('admin/users/create') !!}"><i class="glyphicon glyphicon-plus"></i> &nbsp;Add Users</a></div>
                   <table id="users" class="table table-striped table-bordered table-list-page" data-page-length='5'>
                      <thead>
                        <tr>
                          <th width="20%">Name</th>
                          <th>Email</th>
                          <th>Contact Number</th>
                          <th>Designation</th>  
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach( $users as $key => $value )
                              <tr>
                                <td>{!! $value->name !!}</td>
                                <td>{!! $value->email !!}</td>
                                <td>{!! $value->contact_number !!}</td>
                                <td>
                                    @foreach( $value->roles as $roles )
                                      {!! $roles->display_name !!}
                                    @endforeach
                                </td>
                                <td class="text-center">
                                  <ul class="list list-inline">
                                    <li> <a href="{!! url('admin/users/'.$value->id.'/edit') !!}" data-id="{!! $value->id !!}" data-url="{!! url() !!}" data-token="{!! csrf_token() !!}" ><i class="glyphicon glyphicon-pencil"></i></a> </li>
                                    <li> <a href="#" data-id="{!! $value->id !!}" data-url="{!! url('admin/users/'.$value->id) !!}" data-token="{!! csrf_token() !!}" class="delete-user"><i class="glyphicon glyphicon-trash "></i></a> </li>
                                  </ul>
                                  
                                </td>

                              </tr>

                          @endforeach
                      </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
<div id="deleteuser" class="modal fade" role="dialog" >
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-body">Successfully Deleted User.</div>
  </div>
  </div>
</div>
@stop