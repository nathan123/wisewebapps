@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>

        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">ADD CUSTOMER SPOUSE EDUCATION</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper clearfix">
            <a href="{!! URL::to('admin/customers/'.$customer) !!}">Back</a>
              <h3>Customer Spouse Education Info</h3>
             
              {!! Form::open(array( 'url' => url('admin/customers/storespouseeducation'), 'role'=>'form', 'id'=>'addspouseeducation' )) !!}
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>STATUS</th>
                        <th>NAME OF SCHOOL</th>
                        <th>FROM</th>
                        <th>TO</th>
                        <th>QUALIFICATION</th>
                      </tr>
                    </thead>  
                    <tbody>
                        <tr class="add_education_info">
                        <input type="hidden" name="count" />
                          <td>
                              <div class="form-group">                      
                                {!! Form::select('status1', [
                                   'graduate' => 'Graduate',
                                   'undergraduate' => 'UnderGraduate',
                                   'ongoing' => 'On Going',
                                   'postgrad' => 'Post Graduate',
                                   ], null, ['class' => 'form-control'] ) 
                               !!}                                 
                              </div>
                          </td> 
                          <td> 
                            <div class="form-group">
                              {!! Form::text('name_of_school1',null,['class'=>'form-control required']) !!}
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                                <input type="date" name="from1" class="form-control required" />
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                                <input type="date" name="to1" class="form-control required" />
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              {!! Form::text('qualification1',null,['class'=>'form-control required']) !!}
                            </div>
                          </td>
                        </tr>
                    </tbody>
                </table>

                <div class="form-group">
                   <span class="addeducationfield" style="cursor: pointer;"><i class="glyphicon glyphicon-plus"></i> Add Education</span>
                </div>
                <div class="form-group">
                   <input type="hidden" name="customers_spouse_id" value="{!! $spouse !!}" />
                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                </div>

              {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop