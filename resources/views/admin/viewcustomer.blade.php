@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">VIEW CUSTOMER</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <div class="section-wrapper">
				<!-- Tab panes -->
				<ul class="nav nav-tabs" role="tablist">
			      <li class="active">
			          <a href="#customer" role="tab" data-toggle="tab">
			              <icon class="fa fa-users"></icon> Customers
			          </a>
			      </li>
			      <li><a href="#activity" role="tab" data-toggle="tab">
			          <i class="fa fa-cog"></i> Activity Log
			          </a>
			      </li>
			      <li>
			          <a href="#payment" role="tab" data-toggle="tab">
			              <i class="fa fa-credit-card"></i> Payments
			          </a>
			      </li>
			      <li>
			          <a href="#document" role="tab" data-toggle="tab">
			              <i class="fa fa-bars"></i> Documents
			          </a>
			      </li>
			    </ul>

				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade active in" id="customer">
						<div class="clearfix">
							<div class="col-md-6 text-left">
								<h4>Personal Information 
									@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )

											<a class="small" href="{!! url('admin/customers/'. $customers->id . '/edit') !!}"><i class="glyphicon glyphicon-pencil"></i>edit</a>

									@endif
								</h4>
							</div>
							<div class="col-md-6 text-right status-wrapper">
								@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
									@if( $customers->status != 'approve' && $customers->status != 'disapprove' )
										<button id="approve" data-id="{!! $customers->id !!}" data-token="{!! csrf_token() !!}" data-url="{!! url() !!}" class="btn btn-success" {!! ( $customers->status == 'hold' ) ? 'disabled' : '' !!} >Approve</button>
									@endif

									@if( $customers->status != 'approve' && $customers->status != 'disapprove' )
										<button id="disapprove" data-id="{!! $customers->id !!}" data-token="{!! csrf_token() !!}" data-url="{!! url() !!}" class="btn btn-danger"  {!! ( $customers->status == 'hold' ) ? 'disabled' : '' !!} >Disapprove</button>
									@endif

									@if( $customers->status != 'hold' && $customers->status != 'approve' && $customers->status != 'disapprove' )
										<button id="holdcustomer" data-id="{!! $customers->id !!}" data-token="{!! csrf_token() !!}" data-url="{!! url() !!}" class="btn btn-warning">Hold</button>
									@elseif( $customers->status != 'approve' && $customers->status != 'disapprove' )
										<button id="unholdcustomer" data-id="{!! $customers->id !!}" data-token="{!! csrf_token() !!}" data-url="{!! url() !!}" class="btn btn-warning">Unhold</button>
									@endif
								@endif	
							</div>

							<div class="changestatsmessage" style="display:inline-block;width:100%"></div>
						</div>
							


						<div class="customers-view-info clearfix">
							<div class="col-md-12">

								@if( session('success') )
				           		 	<div class="clearfix"><span class="alert alert-success" style="display: inherit;">{!! session('success') !!}</span></div>
								@endif
									
								<form class="form-inline clearfix" role="form" id="customerinfo">

									<div class="clearfix">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label" for="application_type">Application Type :</label>
												<p class="form-control-static">
													@if( !empty($application) )
														@foreach( $application as $app => $type ) 
															{!! $type->title !!}
														@endforeach
													@else
														N/A
													@endif
												</p>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label" for="application_type">Source of Application :</label>
												<p class="form-control-static">
													{!! $customers->source !!}
												</p>
											</div>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label" for="name">Name:</label>						
											<p class="form-control-static">{!! ucwords( $customers->firstname ) . ' '. ucwords($customers->lastname) !!}</p>										
										</div>

										<div class="form-group">
											<label class="control-label">Middle Name :</label>
											<p class="form-control-static">{!! ucwords( $customers->middle_name ) !!}</p>
										</div>

										<div class="form-group">
											<label class="control-label" for="name">Email:</label>						
											<p class="form-control-static">{!! $customers->email !!}</p>										
										</div>

										<div class="form-group">
											<label class="control-label" for="name">Second Email:</label>						
											<p class="form-control-static">{!! $customers->secondary_email !!}</p>										
										</div>

										<div class="form-group">
											<label class="control-label" for="name">Other Email:</label>						
											<p class="form-control-static">{!! $customers->other_email !!}</p>										
										</div>


										<div class="form-group">
											<label class="control-label" for="name">Date Of birth:</label>						
											<p class="form-control-static">{!! $customers->date_of_birth !!}</p>										
										</div>

										<div class="form-group">
											<label class="control-label" for="name">Gender:</label>						
											<p class="form-control-static">{!! $customers->gender !!}</p>										
										</div>

									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label" for="name">Contact Number:</label>						
											<p class="form-control-static">{!! $customers->contact_number !!}</p>										
										</div>

										<div class="form-group">
											<label class="control-label" for="name">Second Number:</label>						
											<p class="form-control-static">{!! $customers->secondary_contact_number !!}</p>										
										</div>

										<div class="form-group">
											<label class="control-label" for="name">Other Number:</label>						
											<p class="form-control-static">{!! $customers->other_contact_number !!}</p>										
										</div>


										<div class="form-group">
											<label class="control-label" for="name">Profession:</label>						
											<p class="form-control-static">{!! $customers->profession !!}</p>										
										</div>

										<div class="form-group">
											<label class="control-label" for="name">Address:</label>						
											<textarea class="form-control" style="width:60%" rows="5" disabled>{!! $customers->city . ',' . $customers->province . ' ' . $customers->country !!}</textarea>										
										</div>

										<div class="form-group">
											<label class="control-label" for="name">Case Officer: </label>	
											@foreach( $case_officer as $co )
												<p class="form-control-static">{!! $co->name !!}</p>		
											@endforeach
										</div>
										<div class="form-group">
											<label class="control-label" for="name">Status:</label>						
											<p class="form-control-static" >{!! $customers->status !!}</p>		
										</div>

									</div>

									<div class="col-md-12">

										<div class="form-group">
											<label class="control-label" for="name" style="width:100%">Additional Information specifified:</label>						
											<textarea class="form-control" style="width:100%" rows="5" disabled>{!! $customers->meta_description_second !!}</textarea>										
										</div>

										<div class="form-group">
											@if( count($customers_file) > 0 )	
												@foreach( $customers_file as $file )
													<div class="file_name"><a href="{!! url('/uploads/'.$file->name) !!}" class="btn btn-small btn-warning" download> Download CV </a> </div> 
												@endforeach
											@else
												<div class="file_name"> Download  : <label>no file uploaded.</label>
												</div>
											@endif

										</div>

									</div>
								</form>
							</div>								
						</div>
						<hr />
						@if( !empty( $customers->family ) )
							<h4>Family Information 
								@if( Auth::user()->id == $customers->user_id  || Auth::user()->hasRole('admin') )
									<small><a class="" href="{!! url('admin/customers/family/'. $customers->family->id . '/edit') !!}"><i class="glyphicon glyphicon-pencil"></i>edit</a></small>
								@endif
							</h4>
							
							
							<div class="customers-view-info clearfix">
								<form class="form-inline clearfix" role="form">

									<div class="col-md-6">

										<div class="form-group">
											<label class="control-label" for="name">Status:</label>		
											<p class="form-control-static">{!! $customers->family->status !!}</p>					
										</div>
																				

										<div class="form-group">
											<label class="control-label" for="name">Number of Children:</label>						
											<p class="form-control-static">{!! $customers->family->number_of_children !!}</p>										
										</div>
										
									</div>
									
								</form>
							</div>
						@else 
							<h4>Family Information </h4>
							@if( Auth::user()->id == $customers->user_id  || Auth::user()->hasRole('admin') )
								<a href="{!! url('admin/customers/addfamilycomposition/'.$customers->id) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Family Composition</a>
							@endif	
						@endif

						<hr />
						@if( !empty( $customers->education ) )
							<h4>Education Information </h4>
							@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
								<a href="{!! url('admin/customers/addeducation/'.$customers->id) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Education</a>
							@endif
							<div class="customers-view-info clearfix">
								<form class="form-inline clearfix" role="form">
									<div class="col-md-12">

										@foreach( $customers->education as $education )
											<h4>School 
												@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
											 		<small><a class="" href="{!! url('admin/customers/education/'. $education->id . '/edit') !!}"><i class="glyphicon glyphicon-pencil"></i> edit</a></small>
											 	@endif
											 </h4>
												<div class="form-group">
													<label class="control-label" for="name">Status :</label>						
													<p class="form-control-static">{!! $education->status !!}</p>										
												</div>

												<div class="form-group">
													<label class="control-label" for="name">From :</label>						
													<p class="form-control-static">{!! $education->from !!}</p>										
												</div>

												<div class="form-group">
													<label class="control-label" for="name">To :</label>						
													<p class="form-control-static">{!! $education->to !!}</p>										
												</div>

												<div class="form-group">
													<label class="control-label" for="name">Name Of School :</label>						
													<p class="form-control-static">{!! $education->name_of_school !!}</p>										
												</div>

												<div class="form-group">
													<label class="control-label" for="name">Qualification :</label>						
													<p class="form-control-static">{!! $education->qualification !!}</p>										
												</div>

										@endforeach
									</div>
								</form>
							</div>
						@else
							<h4>Education Information </h4>
							@if( Auth::user()->id == $customers->user_id  || Auth::user()->hasRole('admin') )
								<a href="{!! url('admin/customers/addeducation/'.$customers->id) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Education</a>
							@endif
						@endif

						<hr />
						@if( !empty( $customers->work ) )
							<h4>Work History Information  </h4>
							@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
								<a href="{!!  url('admin/customers/addwork/'.$customers->id) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Work</a>
							@endif
							<div class="customers-view-info clearfix">
								<form class="form-inline clearfix" role="form">
									<div class="col-md-12">

										@foreach( $customers->work as $work )
											<h4>Work 
												@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
												 	<small><a class="" href="{!! url('admin/customers/work/'. $work->id . '/edit') !!}"><i class="glyphicon glyphicon-pencil"></i> edit</a></small> 
												@endif
											</h4>												

												<div class="form-group">
													<label class="control-label" for="name">From :</label>						
													<p class="form-control-static">{!! $work->from !!}</p>										
												</div>

												<div class="form-group">
													<label class="control-label" for="name">To :</label>						
													<p class="form-control-static">{!! $work->to !!}</p>										
												</div>

												<div class="form-group">
													<label class="control-label" for="name">Name Of Company :</label>						
													<p class="form-control-static">{!! $work->name_of_company !!}</p>										
												</div>

												<div class="form-group">
													<label class="control-label" for="name">Designation :</label>						
													<p class="form-control-static">{!! $work->designation !!}</p>										
												</div>

										@endforeach
									</div>
								</form>
							</div>
						@else
							<h4>Work History Information </h4>
							@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
								<a href="{!!  url('admin/customers/addwork/'.$customers->id) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Work</a>
							@endif
						@endif

						<hr />
						@if( !empty( $customers->language ) )
							<h4>Language Information </h4>
							@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
								<a href="{!!url('admin/customers/addlanguage/'.$customers->id) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Language</a>
							@endif
							<div class="customers-view-info clearfix">
								<form class="form-inline clearfix" role="form">
									<div class="col-md-12">
										@foreach( $customers->language as $language )
											<h4>Language  
												@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
													<small><a class="" href="{!! url('admin/customers/language/'. $language->id . '/edit') !!}"><i class="glyphicon glyphicon-pencil"></i> edit</a></small> 
												@endif
											</h4>

											<div class="form-group">
												<label class="control-label" for="name">Language :</label>						
												<p class="form-control-static">{!! $language->language !!}</p>										
											</div>

											<div class="form-group">
												<label class="control-label" for="name">Speaking :</label>						
												<p class="form-control-static">{!! round( $language->speaking, 1 ) !!}</p>										
											</div>

											<div class="form-group">
												<label class="control-label" for="name">Listening :</label>						
												<p class="form-control-static">{!! round( $language->listening, 1 ) !!}</p>										
											</div>

											<div class="form-group">
												<label class="control-label" for="name">Reading :</label>						
												<p class="form-control-static">{!! round( $language->reading, 1 ) !!}</p>										
											</div>

											<div class="form-group">
												<label class="control-label" for="name">Writing :</label>						
												<p class="form-control-static">{!! round( $language->writing, 1 ) !!}</p>										
											</div>

										@endforeach
									</div>
								</form>
							</div>
						@else
							<h4>Language Information </h4>
							@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
								<a href="{!!url('admin/customers/addlanguage/'.$customers->id) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Language</a>
							@endif
						@endif

						<hr/>

						@if( count($customers_spouse) > 0 )
							<h4>Customers Spouse</h4>
							
							<div class="customers-view-info clearfix">
								<div class="col-md-12">							
									<form class="form-inline clearfix" role="form">

										@foreach( $customers_spouse as $key => $value )
											<h3>Spouse Information 
												@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
													<small> <a class="" href="{!! url('admin/customers/spouse/'. $value->id . '/edit') !!}"><i class="glyphicon glyphicon-pencil"></i>edit</a></small>
												@endif
											</h3>
											
											<div class="form-group">
												<label class="control-label" for="name">Name:</label>						
												<p class="form-control-static">{!!  ucwords( $value->name ) !!}</p>										
											</div>

											<div class="form-group">
												<label class="control-label" for="name">Email:</label>						
												<p class="form-control-static">{!! $value->email !!}</p>										
											</div>
										
											<div class="form-group">
												<label class="control-label" for="name">Date Of birth:</label>						
												<p class="form-control-static">{!! $value->date_of_birth !!}</p>										
											</div>

											<div class="form-group">
												<label class="control-label" for="name">Contact Number:</label>						
												<p class="form-control-static">{!! $value->contact_number !!}</p>							
											</div>												

											<hr />
											@if( count( $value->education ) > 0 )
												<h4>Spouse Education</h4>

												@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
													<a href="{!! url( 'admin/customers/addspouseeducation/'.$value->id . '/' . $customers->id ) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Spouse Education </a>
												@endif

												@foreach( $value->education as $education )
													<h4>School  
														@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
															<small><a class="" href="{!! url('admin/customers/spouse/education/'. $education->id . '/edit') !!}"><i class="glyphicon glyphicon-pencil"></i> edit</a></small>
														@endif
													</h4>
														
														<div class="form-group">
															<label class="control-label" for="name">Status :</label>						
															<p class="form-control-static">{!! $education->status !!}</p>										
														</div>

														<div class="form-group">
															<label class="control-label" for="name">From :</label>						
															<p class="form-control-static">{!! $education->from !!}</p>										
														</div>

														<div class="form-group">
															<label class="control-label" for="name">To :</label>						
															<p class="form-control-static">{!! $education->to !!}</p>										
														</div>

														<div class="form-group">
															<label class="control-label" for="name">Name Of School :</label>						
															<p class="form-control-static">{!! $education->name_of_school !!}</p>										
														</div>

														<div class="form-group">
															<label class="control-label" for="name">Qualification :</label>						
															<p class="form-control-static">{!! $education->qualification !!}</p>										
														</div>

												@endforeach
											@else
												<h4>Spouse Education</h4>
												@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
													<a href="{!! url( 'admin/customers/addspouseeducation/'.$value->id . '/' . $customers->id ) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Spouse Education </a>
												@endif
											@endif

											<hr />
											@if( count( $value->work ) > 0 )
												<h4>Spouse Work</h4>

												@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
													<a href="{!! url('admin/customers/addspousework/'.$value->id . '/' . $customers->id ) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Spouse Work </a>
												@endif

												@foreach( $value->work as $work )
													<h4>Work 
														@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
													 		<small><a class="" href="{!! url('admin/customers/spouse/work/'. $work->id . '/edit') !!}"><i class="glyphicon glyphicon-pencil"></i> edit</a></small> 
													 	@endif
													</h4>												

													<div class="form-group">
														<label class="control-label" for="name">From :</label>						
														<p class="form-control-static">{!! $work->from !!}</p>										
													</div>

													<div class="form-group">
														<label class="control-label" for="name">To :</label>						
														<p class="form-control-static">{!! $work->to !!}</p>										
													</div>

													<div class="form-group">
														<label class="control-label" for="name">Name Of Company :</label>						
														<p class="form-control-static">{!! $work->name_of_company !!}</p>										
													</div>

													<div class="form-group">
														<label class="control-label" for="name">Designation :</label>						
														<p class="form-control-static">{!! $work->designation !!}</p>										
													</div>
												@endforeach
											@else
												<h4>Spouse Work</h4>
												@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
													<a href="{!! url('admin/customers/addspousework/'.$value->id . '/' . $customers->id ) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Spouse Work </a>
												@endif
											@endif

											<hr />
											@if( count( $value->language ) > 0 )
												<h4> Spouse Language </h4>
												@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
													<a href="{!!url('admin/customers/addspouselanguage/'.$value->id . '/' . $customers->id) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Language</a>
												@endif
													<div class="customers-view-info clearfix">
														<form class="form-inline clearfix" role="form">
															<div class="col-md-12">
																@foreach( $value->language as $language )
																	<h4>Language  
																		@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
																			<small><a class="" href="{!! url('admin/customers/spouse/language/'. $language->id . '/edit') !!}"><i class="glyphicon glyphicon-pencil"></i> edit</a></small> 
																		@endif
																	</h4>

																	<div class="form-group">
																		<label class="control-label" for="name">Language :</label>						
																		<p class="form-control-static">{!! $language->language !!}</p>										
																	</div>

																	<div class="form-group">
																		<label class="control-label" for="name">Speaking :</label>						
																		<p class="form-control-static">{!! $language->speaking !!}</p>										
																	</div>

																	<div class="form-group">
																		<label class="control-label" for="name">Listening :</label>						
																		<p class="form-control-static">{!! $language->listening !!}</p>										
																	</div>

																	<div class="form-group">
																		<label class="control-label" for="name">Reading :</label>						
																		<p class="form-control-static">{!! $language->reading !!}</p>										
																	</div>

																	<div class="form-group">
																		<label class="control-label" for="name">Writing :</label>						
																		<p class="form-control-static">{!! $language->writing !!}</p>										
																	</div>

																@endforeach
															</div>
														</form>
													</div>


											@else

												<h4>Spouse Language</h4>
												@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
													<a href="{!!url('admin/customers/addspouselanguage/'.$value->id . '/' . $customers->id) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Language</a>
												@endif
											@endif

										@endforeach
									</form>
								</div>
							</div>
						@else 
							<h4>Customers Spouse</h4>
							@if( Auth::user()->id == $customers->user_id || Auth::user()->hasRole('admin') )
								<a href="{!!url('admin/customers/addspouse/'.$customers->id) !!}" class=""><i class="glyphicon glyphicon-plus"></i> Add Spouse</a>
							@endif
						@endif

						<hr />
						<div class="clearfix">
							<h4>Comment</h4>
							<div class="col-md-12">

								<form role="form" class="form-horizontal" id="addcomment" method="post" action="{!! url() !!}">

									<div class="form-group">
										<label>Add Comment</label>
										<textarea row="5" col="5" name="activity" class="form-control"></textarea>
									</div>

									<div class="form-group">
										<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
										<input type="hidden" name="user_id" value="{!! $customers->user_id !!}" />
										<input type="hidden" name="customers_id" value="{!! $customers->id !!}" />
										<input type="submit" name="submit" class="btn btn-warning" value="Send">
									</div>
								</form>
							</div>
						</div>
							
					</div>

					<div role="tabpanel" class="tab-pane fade" id="activity">
						<h4>Activity Log</h4>

						<table id="customers-activity" class="table table-striped table-bordered table-list-page" data-page-length='5'>
	                      <thead>
	                        <tr>
	                          <th width="">Name</th>
	                          <th>User</th>
	                          <th width="">Activity</th>	                          
	                          <th width="">Date Created</th>
	                          <th>Case Officer</th>
	                        </tr>
	                      </thead>
	                      <tbody>	 

	                        @foreach( $customers_activity_log as $cust => $activity )

	                          <tr>
	                              <td>{!! ucfirst( $activity->customers->firstname ) . ' ' . ucfirst( $activity->customers->lastname ) !!}</td>
	                              <td> 

	                              	@if( !is_null( $activity->editor ) )

	                              		{!! $activity->editor !!}

	                              	@endif

	                              </td>
	                              <td>{!! $activity->activity !!}</td>
	                              <td>{!! $activity->created_at !!}</td>
	                              <td>{!!ucfirst(  $activity->user->name ) !!}</td>
	                          </tr>                          
	                        @endforeach                     
	                         
	                      </tbody>
	                   </table>
					</div>

					<div role="tabpanel" class="tab-pane fade" id="payment">
						<h4>Payment</h4>
						<div class="clearfix">

							@if( Auth::user()->hasRole('accounting') || Auth::user()->hasRole('admin') )
								<div class="col-md-12">						
									<form role="form" class="form-horizontal" id="addpayment" method="post" action="{!! url('admin/customers/addpayment') !!}">
										<div class="clearfix">
											<div class="col-md-6">
												<div class="form-group">

													<label>Payment Type:</label>
													
													<select class="form-control" name="payment_type" required>
														<option value="">Select Payment Type</option>
														<optgroup label="Consultants Fee">
															<option value="Consultants Fee Full Payment">Consultants Fee Full Payment</option>
															<optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;Partial Payment">													
																<option value="Consultants Fee Partial New Payment Scheme" >&nbsp;&nbsp;&nbsp;&nbsp;Partial Payment New Payment Scheme</option>		
																<option value="Consultants Fee CAD $1,250.00 - 1 Week Only">&nbsp;&nbsp;&nbsp;&nbsp;Partial Payment CAD $1,250.00 - 1 Week Only</option>	
															</optgroup>														
														</optgroup>
														<optgroup label="Registration Fee">
															<option value="Registration Fee Full Payment" >Registration Fee Full Payment</option>																			
															<option value="Registration Fee Partial Payment">Registration Fee Partial Payment</option>	
														</optgroup>
														<option value="IELTS Review">IELTS Review</option>
														<option value="French Review">French Review</option>
														<option value="IELTS Exam">IELTS Exam</option>
														<option value="French Exam">French Exam</option>
														<optgroup label="Courier">
															<option value="Courier Single">Courier Single</option>																			
															<option value="Courier Married / De-Facto">Courier Married / De-Facto</option>	
														</optgroup>
														<optgroup label="Notarial">
															<option value="Notarial Single">Notarial Single</option>																			
															<option value="Notarial Married / De-Facto">Notarial Married / De-Facto</option>	
														</optgroup>
														<option value="Demand Draft">Demand Draft</option>
														<option value="Printing">Printing</option>
														<option value="Scanning">Scanning</option>
														<option value="Photocopying">Photocopying</option>
														<option value="Airline Tickets">Airline Tickets</option>
														<option value="others">Others</option>
													</select>

												</div>

												<div class="form-group">
													<label>Amount:</label>
													<input type="text" name="amount" class="form-control" required/>
												</div>
											</div>

											

											<div class="col-md-12">
												<div class="form-group">

													<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
													@foreach( $case_officer as $co )
														<input type="hidden" name="user_id" value="{!! $co->id !!}" />
													@endforeach
													<input type="hidden" name="customers_id" value="{!! $customers->id !!}" />
													<input type="submit" name="submit" class="btn btn-warning" value="Add Payment">

												</div>
											</div>
										</div>
									</form>
								</div>	
							@endif

							<div class="col-md-12">
						
								<table class="table table-striped" id="customerpayments" data-page-length='5'>
									<thead>
										<tr>
											<th>Payment Type</th>
											<th>Amount</th>
											<th>Date</th>
											<th>User</th>
										</tr>
									</thead>
									<tbody>
										@foreach( $payment as $p => $payment )
											<tr>
												<td>{!! $payment->title !!}</td>
												<td>{!! $payment->amount !!}</td>
												<td>{!! $payment->created_at !!}</td>
												<td>{!! $payment->user->name !!}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							
						</div>
					</div>

					<div role="tabpanel" class="tab-pane fade" id="document">
						<h4>Documents</h4>
						<div class="clearfix">
							<div class="col-md-12">
								<ul class="list list-inline">
									@foreach( $customers->documents as $docs )
										<li>
											<div class="checkbox">
										    	<label>
										    		@if( $docs->pivot->is_available == false )
										      			<input {!! ( Auth::user()->hasRole('document') || Auth::user()->hasRole('admin') ) ? '' : 'disabled' !!} type="checkbox" name="customers_docs" data-customersid="{!! $docs->pivot->customers_id !!}" data-documentsid="{!! $docs->pivot->documents_id !!}" data-token="{!! csrf_token() !!}" data-url="{!! url('admin/customers/hasdocument') !!}" /> 
										      		@else
										      			<input {!! ( Auth::user()->hasRole('document') || Auth::user()->hasRole('admin') ) ? '' : 'disabled' !!}  type="checkbox" name="customers_docs" data-customersid="{!! $docs->pivot->customers_id !!}" data-documentsid="{!! $docs->pivot->documents_id !!}" data-token="{!! csrf_token() !!}" data-url="{!! url('admin/customers/removedocument') !!}" checked/> 
										      		@endif
										      		{!! $docs->title !!}
										    	</label>
										  	</div>
										</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div>                
            </div>
        </div>
    </div>


<div id="hasdocmodal" class="modal fade" role="dialog" >
<div class="modal-dialog" role="document">
<div class="modal-content">
	<div class="modal-header clearfix">
    	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  	</div>
	<div class="modal-body"></div>
	</div>
	</div>
</div>

@stop
