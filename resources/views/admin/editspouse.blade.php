@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">EDIT CUSTOMER</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
            	<a href="{!! URL::to('/admin/customers/'.$customers_spouse->customers_id) !!}">Back</a>
            	
            	@if( session('success') )
           		 	<div class="clearfix"><span class="alert alert-success" style="display: inherit;">{!! session('success') !!}</span></div>
				@endif

				<h2>Edit Customers Information</h2>
					<div class="customers-info clearfix">
						<div class="col-md-8">
							{!! Form::model( $customers_spouse,['method' => 'PATCH','route'=>['admin.customers.spouse.update',$customers_spouse->id],  'id'=>'editspouse' ]) !!}
								
			                	<div class="form-group">
				                    {!! Form::label('name', 'Name:') !!}
				                    {!! Form::text('name',null,['class'=>'form-control required']) !!}
				                </div>
				                <div class="form-group">
				                    {!! Form::label('email', 'Email:') !!}
				                    {!! Form::email('email',null,['class'=>'form-control required']) !!}
				                </div>
				                <div class="form-group">
				                    {!! Form::label('date_of_birth', 'Date of Birth:') !!}
				                    <input type="date" name="date_of_birth" class="form-control" value="{!! $customers_spouse->date_of_birth !!}" />
				                </div>

				                <div class="form-group">
				                    <div class="clearfix">
			                            <div class="col-md-4">
			                               <label>Type:</label>
			                               <select class="form-control" name="primarynumtype">
			                                 <option>Select Type</option>
			                                 <option value="m">Mobile</option>
			                                 <option value="t">Telephone</option>
			                               </select>
			                            </div>
			                            <div class="col-md-7">
			                              {!! Form::label('contact_number', 'Contact Number:') !!}
			                              {!! Form::text('contact_number',null,['class'=>'form-control']) !!}
			                            </div>
			                          </div>
				                </div>						               
				            			   
							    <div class="form-group clearfix">
							    	{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
							    </div>
								
						    {!! Form::close() !!}
						</div>						
					</div>
				</div>
                
            </div>
        </div>
    </div>
@stop