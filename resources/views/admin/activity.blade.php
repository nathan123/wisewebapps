@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">CUSTOMERS</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
               <div class="panel panel-default">
                  <div class="panel-heading">
                    <h5>Activity Logs &nbsp;&nbsp;&nbsp;
                      @if( Auth::user()->hasRole('admin') )
                        <small class="text-right"><a href="#" class="readall" data-url="{!! url() !!}" data-token="{!! csrf_token() !!}">Click</a>  Here To Read All</small>
                      @endif
                    </h5>
                  </div>
                   <table id="logs" class="table table-striped table-bordered table-list-page" data-page-length='5'>
                      <thead>
                        <tr>
                          <th width="20%">Name</th>
                          <th>User</th>
                          <th width="20%">Activity</th>
                          <th width="">Date Created</th>
                          <th>Case Officer</th>

                        </tr>
                      </thead>
                      <tbody>

                        @foreach( $activity as $key => $value )

                          
                              @if( $value->is_read == 0  )
                                <tr class="bold">
                              @else 
                                <tr>
                              @endif
                                <td>{!! ( !is_null( $value->customers ) ) ? ucfirst( $value->customers->firstname ) . ' ' . ucfirst( $value->customers->lastname ) : '' !!}</td>
                                <td>{!! $value->editor !!}</td>
                                <td>{!! $value->activity !!}</td>
                                <td>{!! $value->created_at !!}</td>
                                <td>{!! $value->user->name !!}</td>
                              
                          </tr>                          
                        @endforeach                     
                         
                      </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>

@stop