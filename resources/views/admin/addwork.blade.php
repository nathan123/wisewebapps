@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">ADD WORK HISTORY</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
            <a href="{!! URL::to('admin/customers/'.$customers) !!}">Back</a>
               <h3>Customer Work History Info</h3>
             
              {!! Form::open(array( 'url' => url('admin/customers/storework'), 'role'=>'form', 'id'=>'addwork' )) !!}
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Designation</th>
                        <th>FROM</th>
                        <th>TO</th>
                        <th>Name Of Company</th>
                      </tr>
                    </thead>  
                    <tbody>
                        <tr class="add_work_history">
                        <input type="hidden" name="count" />                      
                          <td> 
                            <div class="form-group">
                              {!! Form::text('designation1',null,['class'=>'form-control required']) !!}
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                                <input type="date" name="from1" class="form-control required" />
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                                <input type="date" name="to1" class="form-control required" />
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              {!! Form::text('name_of_company1',null,['class'=>'form-control required']) !!}
                            </div>
                          </td>
                        </tr>
                    </tbody>
                </table>
                <div class="form-group">
                   <span class="addworkhistory" style="cursor: pointer;"><i class="glyphicon glyphicon-plus"></i> Add Work History</span>
                </div>
                <div class="form-group">
                   <input type="hidden" name="customers_id" value="{!! $customers !!}" />
                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                </div>

              {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop