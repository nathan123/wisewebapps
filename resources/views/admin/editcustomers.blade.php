@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">EDIT CUSTOMER</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
            		
				<a href="{!! URL::previous() !!}">Back</a>
				<h2>Edit Customers Information</h2>
					<div class="customers-info clearfix">
						<div class="col-md-12">
							{!! Form::model( $customers,['method' => 'PATCH','route'=>['admin.customers.update',$customers->id],  'id'=>'editcustomers' ]) !!}
								@if( Auth::user()->hasRole('case_officer') || Auth::user()->hasRole('admin') )
									<div class="form-group">
					                   {!! Form::label('application', 'Application Type:') !!}
					                  <select class="form-control" name="applications_id">
					                      <option value="">Select Type:</option>
					                      @foreach( $application as $k => $v )
					                        <option value="{!! $v->id !!}" <?php if($customers->applications_id == $v->id){ echo 'selected';} ?> >{!! $v->title !!}</option>
					                      @endforeach
					                   </select>
					                </div>
					            @endif

					            @if( Auth::user()->hasRole('receptionist') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('case_officer') )
						            <div class="form-group">
						            	<select class="form-control" name="source" required> 
						            		<option>Please Select Source Of Application</option>
						            		<option value="FB" {!! ($customers->source == 'FB') ? 'selected' : '' !!} >FB</option>
						            		<option value="Email" {!! ($customers->source == 'Email') ? 'selected' : '' !!} >Email</option>
						            		<option value="Walk-in" {!! ($customers->source == 'Walk-in') ? 'selected' : '' !!} >Walk-in</option>
						            		<option value="Event" {!! ($customers->source == 'Event') ? 'selected' : '' !!} >Event</option>
						            		<option value="Website" {!! ($customers->source == 'Website') ? 'selected' : '' !!} >Website</option>
						            	</select>
						            </div>
						        @endif
						        
			                	<div class="form-group">
				                    {!! Form::label('name', 'Firstname:') !!}
				                    {!! Form::text('firstname',null,['class'=>'form-control', 'required']) !!}

				                    {!! Form::label('middle_name', 'Middle Name:') !!}
				                    {!! Form::text('middle_name',null,['class'=>'form-control']) !!}

				                    {!! Form::label('name', 'Lastname:') !!}
				                    {!! Form::text('lastname',null,['class'=>'form-control', 'required']) !!}
				                </div>
				                <div class="form-group">	                    
				                    <div class="clearfix">
					                    <div class="col-md-10">
					                        {!! Form::label('email', 'Email:') !!}
					                    	{!! Form::email('email',null,['class'=>'form-control','required']) !!}
					                    </div>
					                  	
					                  	<div class="col-md-5">
					                        {!! Form::label('secondary_email', 'Second Email:') !!}
					                    	{!! Form::email('secondary_email',null,['class'=>'form-control']) !!}
					                    </div>

					                    <div class="col-md-5">
					                        {!! Form::label('other_email', 'Other Email:') !!}
					                    	{!! Form::email('other_email',null,['class'=>'form-control']) !!}
					                    </div>
					                    
				                  	</div>
				                </div>
				                <div class="form-group">
				                    {!! Form::label('date_of_birth', 'Date of Birth:') !!}
				                    <input type="date" name="date_of_birth" class="form-control" value="{!! $customers->date_of_birth !!}" />
				                </div>
				                <div class="form-group">
				                    {!! Form::label('gender', 'Gender:') !!} <br />
				                    Female : <input type="radio" name="gender" value="F" <?php if($customers->gender == 'F') echo 'checked'; ?> />
				                    Male : <input type="radio" name="gender" value="M" <?php if($customers->gender == 'M') echo 'checked'; ?> >
				                </div>
				                <div class="form-group">
				                	<div class="clearfix">
				                		<div class="col-md-2">			                			
				                           <label>Type:</label>
				                           <select class="form-control" name="primarynumtype">
				                             <option>Select Type</option>
				                             <option value="m">Mobile</option>
				                             <option value="t">Telephone</option>
				                           </select>
				                		</div>
					                    <div class="col-md-6">
						                    {!! Form::label('contact_number', 'Contact Number:') !!}
						                    {!! Form::text('contact_number',null,['class'=>'form-control']) !!}
						                </div>
				                    </div>
				                    <div class="clearfix">
			                        <div class="col-md-2">
			                           <label>Type:</label>
			                           <select class="form-control" name="secondarynumtype">
			                             <option>Select Type</option>
			                             <option value="m">Mobile</option>
			                             <option value="t">Telephone</option>
			                           </select>
			                        </div>
			                         <div class="col-md-6">
			                          {!! Form::label('secondary_contact_number', 'Second Contact Number:') !!}
			                          {!! Form::text('secondary_contact_number',null,['class'=>'form-control']) !!}
			                        </div>
			                      </div>
			                      <div class="clearfix">
			                        <div class="col-md-2">
			                           <label>Type:</label>
			                           <select class="form-control" name="othernumtype">
			                             <option>Select Type</option>
			                             <option value="m">Mobile</option>
			                             <option value="t">Telephone</option>
			                           </select>
			                        </div>
			                         <div class="col-md-6">
			                          {!! Form::label('other_contact_number', 'Third Contact Number:') !!}
			                          {!! Form::text('other_contact_number',null,['class'=>'form-control']) !!}
			                        </div>
			                      </div>
				                </div>
				                <div class="form-group">
				                    {!! Form::label('profession', 'Profession:') !!}
				                    {!! Form::text('profession',null,['class'=>'form-control']) !!}
				                </div>

				                <div class="form-group">
				                	@if( count( $customers_file ) > 0 )
					                	@foreach( $customers_file as $key => $value )
					                		<input type="hidden" name="file_id" value="{!! $value->id !!}" />

					                		<div class="file_name"><a href="{!! url('/upload/'.$value->name) !!}" class="btn btn-warning" download>Download CV</a> </div> 
					                	@endforeach
				                	@else 
				                		 <div class="file_name"> Download : <label>no file uploaded.</label> </div> 
				                	@endif
				                </div>
				                	
				                <div class="form-group">
				                    {!! Form::label('changefile', 'Change / Upload Resume :') !!}
				                    {!! Form::file('file',['class'=>'form-control','id'=>'changefile', 'data-url' => url() ]) !!}
				                </div>

				                 <div class="form-group">
				                    {!! Form::label('city', 'City:') !!}
				                    {!! Form::text('city',null,['class'=>'form-control']) !!}
				                </div>
				                <div class="form-group">
				                    {!! Form::label('province', 'Province:') !!}
				                    {!! Form::text('province',null,['class'=>'form-control']) !!}
				                </div>
				                <div class="form-group">
				                    {!! Form::label('country', 'Country:') !!}
				                    {!! Form::text('country',null,['class'=>'form-control']) !!}
				                </div>

				                <div class="form-group">
				                    {!! Form::label('country', 'Country:') !!}
				                    <select name="country" class="form-control">
				                        <option>Please Select Country</option>
				                        <option value="Philippines" {!! ( $customers->country == 'Philippines' ) ? 'selected' : '' !!} >Philippines</option>
				                        <option value="Canada" {!! ( $customers->country == 'Canada' ) ? 'selected' : '' !!} >Canada</option>
				                        <option value="New Zealand" {!! ( $customers->country == 'New Zealand' ) ? 'selected' : '' !!} >New Zealand</option>
				                    </select>
				                </div>

				                <div class="form-group">
				                  {!! Form::label('user', 'Case Officer:') !!}
				                    <select class="form-control required" name="user_id" required>						                    		
						                      @foreach( $users as $key => $value )
						                      		@foreach( $value->roles as $roles )
                  										@if( $roles->name == 'case_officer' )
								                      		@if( $customers->user_id === $value->id)
								                      			<option value="{!! $value->id !!}" selected>{!! $value->name !!}</option>
								                      		 @endif
								                      		<option value="{!! $value->id !!}">{!! $value->name !!}</option>
								                      	 @endif	
								                    @endforeach
						                      @endforeach
				                    </select>
				                </div>				   

				                <div class="form-group">
				                    {!! Form::label('meta_description_first', ' Any Self-Employed Profession By Applicant Or Spouse During Last 5 Years:') !!}
				                    {!! Form::textarea('meta_description_first',null,['class'=>'form-control', 'rows' => 2]) !!}
				                </div>
				                
				               
				                 <div class="form-group">
				                    {!! Form::label('meta_description_second', 'Additional Information you want to add or specify:') !!}
				                    {!! Form::textarea('meta_description_second',null,['class'=>'form-control', 'rows' => 2]) !!}
				                </div>
                
							    <div class="form-group clearfix">
							    	
							    	{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!} 
							    	
							    </div>
								
						    {!! Form::close() !!}
						</div>
						
					</div>
            </div>
        </div>
    </div>
@stop