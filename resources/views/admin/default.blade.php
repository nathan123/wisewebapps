<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>WISE WEB APPS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="apple-touch-icon" href="/favicon.png">
    <!-- <link rel="shortcut icon" href="{{ asset('favicon.png') }}"> -->
<!-- load bootstrap from a cdn -->

    {!! HTML::style('css/bootstrap.min.css') !!}
    {!! HTML::style('css/bootstrap-theme.min.css') !!}
    {!! HTML::style('css/font-awesome.min.css') !!}     
    {!! HTML::script('js/modernizr-2.8.3-respond-1.4.2.min.js') !!}
    {!! HTML::style('css/jquery-ui.css') !!}
    {!! HTML::style('css/jquery.dataTables.min.css') !!} 
    {!! HTML::style('css/main.css') !!}  

     {!! HTML::script('js/jquery-1.11.2.min.js') !!}
     {!! HTML::script('js/jquery-ui.js') !!}
    <!-- script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script -->
    <!-- script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script -->
    <script type="text/javascript"> var global_url = "{!! url()!!}"; </script>
</head>
<body>  
    <a href="" id="top"></a>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><!-- WISE WEB APPS ADMIN --></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown" style="border: 1px solid #ddd;"><i class="glyphicon glyphicon-user"></i> {!! Auth::user()->name !!} <strong class="caret"></strong></a>
                            <div class="dropdown-menu" style="padding: 15px; padding-bottom: 0px;">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#"><i class="glyphicon glyphicon-user"></i>  Profile</a></li>
                                    <li><a href="#"><i class="glyphicon glyphicon-cog"></i>  Settings</a></li>
                                    <li><a href="{!! url('/auth/logout') !!}"><i class="glyphicon glyphicon-lock"></i>  Logout</a></li>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


		<div class="container-fluid">
            @yield('content')
    	</div>
        
        <div id="myModal" class="modal fade" role="dialog" >
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body"></div>
            </div>
            </div>
        </div>

        <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
        <script src="{!! asset('js/main.js') !!}"></script>
        <script src="{!! asset('js/customers.js') !!}"></script>
        <script src="{!! asset('js/jquery.maskedinput.min.js') !!}"></script>
        <script src="{!! asset('js/jquery.dataTables.columnFilter.js') !!}"></script>
        <script src="{!! asset('js/jquery.dataTables.min.js') !!}"></script>
        <script src="{!! asset('js/jquery.formatCurrency-1.4.0.min.js') !!}"></script>
        <script src="{!! asset('js/jspdf.min.js') !!}"></script>
        <!-- script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script -->
</body>
</html>