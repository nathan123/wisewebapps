@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">

             <h2 class="">EDIT CUSTOMER</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	
            <div class="section-wrapper">
            	<a href="{!! URL::to('admin/customers/'.$custfamily->customers_id) !!}">Back</a>
           		@if( session('success') )
           		 	<div class="clearfix"><span class="alert alert-success" style="display: inherit;">{!! session('success') !!}</span></div>
				@endif

				<h2>Edit Customers Family Information</h2>
				<div class="customers-info clearfix">
					<div class="col-md-12">
						{!! Form::model( $custfamily,['method' => 'PATCH','route'=>['admin.customers.family.update',$custfamily->id],  'id'=>'editcustomersfamily' ]) !!}
					
		                	<div class="form-group">
		                		 {!! Form::label('status', 'Status :') !!}
				                  {!! Form::select('status', [
				                     'single' => 'Single',
				                     'married' => 'Married',
				                     'annulled' => 'Annulled',
				                     'widow' => 'Widow',
				                     ], null, ['class' => 'form-control'] ) 
				                 !!}
			                </div>
			                <div class="form-group">
			                   {!! Form::label('number_of_children', 'Number Of Children:') !!}
      						   {!! Form::text('number_of_children',null,['class'=>'form-control number']) !!}
			                </div>
			               		   

						    <div class="form-group">
						        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
						    </div>
							
					    {!! Form::close() !!}
					</div>
				</div>

                
            </div>
        </div>
    </div>
@stop