@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">ADD USERS</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
                 <a href="{!! URL::to('/admin/users/') !!}">Back</a>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if( session('success') )
                        <div class="clearfix"><span class="alert alert-success" style="display: inherit;">{!! session('success') !!}</span></div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{!! url('/admin/users/store') !!}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{!! old('name') !!}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">E-Mail Address</label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{!! old('email') !!}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Role Type:</label>
                            <div class="col-md-6">
                                <select class="form-control" name="role">
                                    <option value='' selected>Select Role</option>
                                    <option value='2'>Receptionist</option>
                                    <option value='3'>Case Officer</option>
                                    <option value='4'>Accounting</option>
                                    <option value='5'>Document Handler</option>
                                    <option value='6'>Encoder</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="clearfix">
                                <div class="col-md-2 col-md-offset-2">
                                    <label class="control-label">Type:</label>
                                        <select class="form-control" name="num_type">
                                            <option>Select Type</option>
                                            <option value="m">Mobile</option>
                                            <option value="t">Telephone</option>
                                        </select>
                                </div>
                                
                                <div class="col-md-6">
                                    <label class="control-label">Contact Number</label>
                                    <input type="text" class="form-control" name="contactnumber">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add Employee
                                </button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
@stop