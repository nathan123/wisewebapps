@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            @include('includes.sidebar')
        </div>

        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">ADD CUSTOMER</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper clearfix">
              <h3>Personal Information</h3>
              @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <strong>Whoops!</strong> There were some problems with your input.<br><br>
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              {!! Form::open(array( 'url' => url('admin/customers/store'), 'role'=>'form', 'id'=>'addcustomer','files'=>true )) !!}

                @if( Auth::user()->hasRole('case_officer') || Auth::user()->hasRole('admin') )
                  <div class="form-group">
                     {!! Form::label('application', 'Application Type:') !!}
                     <select class="form-control required" name="applications_id">
                        <option value="">Select Type:</option>
                        @foreach( $application as $k => $v )
                          <option value="{!! $v->id !!}">{!! $v->title !!}</option>
                        @endforeach
                     </select>
                  </div>
                @endif

                @if( Auth::user()->hasRole('receptionist') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('encoder') )
                   <div class="form-group">
                    {!! Form::label('source', 'Source of Application:') !!}
                    <select class="form-control" name="source" required> 
                      <option value="">Please Select Source Of Application</option>
                      <option value="FB">FB</option>
                      <option value="Email">Email</option>
                      <option value="Walk-in">Walk-in</option>
                      <option value="Event">Event</option>
                      <option value="Website">Website</option>
                    </select>
                  </div>                     
                @endif
                
                <div class="form-group">
                    {!! Form::label('firstname', 'Firstname:') !!}
                    {!! Form::text('firstname',null,['class'=>'form-control','required']) !!}
                    <br/>
                    {!! Form::label('middle_name', 'Middle Name:') !!}
                    {!! Form::text('middle_name',null,['class'=>'form-control']) !!}
                    <br/>
                    {!! Form::label('lastname', 'Lastname:') !!}
                    {!! Form::text('lastname',null,['class'=>'form-control', 'id' => 'addlastname', 'data-token' => csrf_token(), 'data-url' => url('admin/customers/searchname'),'required' ]) !!}
                </div>
                <div class="form-group">
                  <div class="clearfix">
                      <div class="col-md-10">
                        {!! Form::label('email', 'Primary email:') !!}
                        {!! Form::email('email',null,['class'=>'form-control','required']) !!}
                      </div>
                       <div class="col-md-5">
                        {!! Form::label('secondary_email', 'Second email:') !!}
                        {!! Form::email('secondary_email',null,['class'=>'form-control']) !!}
                      </div>
                       <div class="col-md-5">
                        {!! Form::label('other_email', 'Third email:') !!}
                        {!! Form::email('other_email',null,['class'=>'form-control']) !!}
                      </div>
                  </div>
                </div>
                <div class="form-group">
                    {!! Form::label('date_of_birth', 'Date of Birth:') !!}
                    <input type="date" name="date_of_birth" class="form-control" style="width:40%;" />
                </div>
                <div class="form-group">
                    {!! Form::label('gender', 'Gender:') !!} <br />
                    Male :<!--  <input type="radio" name="gender" value="F" /> --> {!! Form::radio('gender', 'M') !!}
                    Female :<!--  <input type="radio" name="gender" value="M" /> --> {!! Form::radio('gender', 'F') !!}
                    <br />
                    <span class="gender_error"></span>
                </div> 
                <div class="form-group">
                     <div class="clearfix">
                        <div class="col-md-2">
                           <label>Type:</label>
                           <select class="form-control" name="primarynumtype">
                             <option>Select Type</option>
                             <option value="m">Mobile</option>
                             <option value="t">Telephone</option>
                           </select>
                        </div>
                        <div class="col-md-6">
                          {!! Form::label('contact_number', 'Contact Number:') !!}
                          {!! Form::text('contact_number',null,['class'=>'form-control']) !!}
                        </div>
                      </div>
                      <div class="clearfix">
                        <div class="col-md-2">
                           <label>Type:</label>
                           <select class="form-control" name="secondarynumtype">
                             <option>Select Type</option>
                             <option value="m">Mobile</option>
                             <option value="t">Telephone</option>
                           </select>
                        </div>
                         <div class="col-md-6">
                          {!! Form::label('secondary_contact_number', 'Second Contact Number:') !!}
                          {!! Form::text('secondary_contact_number',null,['class'=>'form-control']) !!}
                        </div>
                      </div>
                      <div class="clearfix">
                        <div class="col-md-2">
                           <label>Type:</label>
                           <select class="form-control" name="othernumtype">
                             <option>Select Type</option>
                             <option value="m">Mobile</option>
                             <option value="t">Telephone</option>
                           </select>
                        </div>
                         <div class="col-md-6">
                          {!! Form::label('other_contact_number', 'Third Contact Number:') !!}
                          {!! Form::text('other_contact_number',null,['class'=>'form-control']) !!}
                        </div>
                      </div>
                </div>
                <div class="form-group">
                    {!! Form::label('profession', 'Profession:') !!}
                    {!! Form::text('profession',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('file', 'Upload resume') !!}
                    {!! Form::file('file',['class'=>'form-control','id'=>'file']) !!}
                </div>
                 <div class="form-group">
                    {!! Form::label('city', 'City:') !!}
                    {!! Form::text('city',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('province', 'Province:') !!}
                    {!! Form::text('province',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('country', 'Country:') !!}
                    <select name="country" class="form-control">
                        <option>Please Select Country</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Canada">Canada</option>
                        <option value="New Zealand">New Zealand</option>
                    </select>
                </div>
                <div class="form-group">
                  {!! Form::label('user', 'Case Officer:') !!}
                    <select class="form-control required" name="user_id" required>
                      <option value="">Please Select Case Officer</option>
                      @foreach( $users as $key => $value )

                        @foreach( $value->roles as $roles )
                          @if( $roles->name == 'case_officer' )
                            <option value="{!! $value->id !!}">{!! $value->name !!}</option>
                          @endif
                        @endforeach

                      @endforeach
                    </select>
                </div>
                
                @if( Auth::user()->hasRole('case_officer') || Auth::user()->hasRole('admin') )
                  <div class="form-group">
                      {!! Form::label('meta_description_first', ' Any Self-Employed Profession By Applicant Or Spouse During Last 5 Years:') !!}
                      {!! Form::textarea('meta_description_first',null,['class'=>'form-control', 'rows' => 2]) !!}
                  </div>
                @endif 
                
                 <div class="form-group">
                    {!! Form::label('meta_description_second', 'Additional Information you want to add or specify:') !!}
                    {!! Form::textarea('meta_description_second',null,['class'=>'form-control', 'rows' => 2]) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                </div>
              {!! Form::close() !!}
            </div>
        </div>
    </div>

<div id="addcustomermodal" class="modal fade" role="dialog" >
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-header clearfix">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  </div>
  <div class="modal-body">Succesfully Added Customer.</div>
  </div>
  </div>
</div>

@stop