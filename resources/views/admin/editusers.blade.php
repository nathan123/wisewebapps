@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">EDIT USER</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
            		
				<a href="{!! URL::to('admin/users') !!}">Back</a>
				<h2>Edit Users Information</h2>
					<div class="customers-info clearfix">
						<div class="col-md-12">
							{!! Form::model( $user,['method' => 'PATCH','route'=>['admin.users.update',$user->id],  'id'=>'edituser' ]) !!}
								
								<div class="form-group">
									{!! Form::label('name', 'Name:') !!}
				                    {!! Form::text('name',null,['class'=>'form-control required']) !!}
								</div>

								<div class="form-group">
									{!! Form::label('email', 'Email:') !!}
				                    {!! Form::email('email',null,['class'=>'form-control required']) !!}
								</div>

								<div class="form-group">
									<label class="control-label">Password: **** <a href="{!! url('admin/users/password/'.$user->id) !!}">(edit)</a></label>
								</div>

								<div class="form-group">
		                            <div class="clearfix">
		                                <div class="col-md-2">
		                                    <label class="control-label">Type:</label>
		                                        <select class="form-control" name="num_type">
		                                            <option>Select Type</option>
		                                            <option value="m">Mobile</option>
		                                            <option value="t">Telephone</option>
		                                        </select>
		                                </div>
		                                
		                                <div class="col-md-6">
		                                    <label class="control-label">Contact Number</label>
		                                    <input type="text" class="form-control" name="contactnumber" value="{!! $user->contact_number !!}">
		                                </div>
		                            </div>
		                        </div>

		                        <div class="form-group">
		                            <label>Role Type:</label>		                           
	                                <select class="form-control" name="role">
	                                    <option value='' selected>Select Role</option>
	                                    <option value='2' <?php if(  $user->role->role_id  == 2 ){echo 'selected';}?>>Receptionist</option>
	                                    <option value='3' <?php if(  $user->role->role_id  == 3 ){echo 'selected';}?>>Case Officer</option>
	                                    <option value='4' <?php if(  $user->role->role_id  == 4 ){echo 'selected';}?>>Accounting</option>
	                                    <option value='5' <?php if(  $user->role->role_id  == 5 ){echo 'selected';}?>>Document Handler</option>
	                                    <option value='6' <?php if(  $user->role->role_id  == 6 ){echo 'selected';}?>>Encoder</option>
	                                </select>
		                        </div>

							    <div class="form-group clearfix">
							    	
							    	{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!} 
							    	
							    </div>
								
						    {!! Form::close() !!}
						</div>
						
					</div>
            </div>
        </div>
    </div>
@stop