@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">DOCUMENTS</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
               {!! Form::open(array( 'url' => url('admin/storedocument'), 'role'=>'form', 'id'=>'addcustdoc' )) !!}

                    <div class="form-group">
                      {!! Form::label('title', 'Document Name:') !!}
                      {!! Form::text('title',null,['class'=>'form-control', 'required']) !!}
                    </div>

                    <div class="form-group">
                      {!! Form::label('description', 'Description:') !!}
                      {!! Form::textarea('description',null,['class'=>'form-control', 'rows' => 2]) !!}
                    </div>

                    <div class="form-group">
                       {!! Form::label('application', 'Application Type:') !!}
                       <select class="form-control" name="application_id" required>
                          <option value="">Select Type:</option>
                          @foreach( $application as $k => $v )
                            <option value="{!! $v->id !!}">{!! $v->title !!}</option>
                          @endforeach
                       </select>
                    </div>

                    <div class="form-group">
                      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>

                {!! Form::close() !!}

                <h3>Document List</h3>
                <table class="table table-striped" id="customerpayments" data-page-length='5'>
                  <thead>
                    <tr>
                      <th>Document Type</th>
                      <th>Description</th>
                      <th>Application Type</th>
                      <th>Created At</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach( $documents as $d => $docs )
                      <tr>
                        <td>{!! $docs->title !!}</td>
                        <td>{!! $docs->description !!}</td>
                       <td>{!! $docs->application->title !!}</td>
                        <td>{!! $docs->created_at !!}</td>
                        
                      </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
        </div>
    </div>
<div id="adddocument" class="modal fade" role="dialog" >
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-body">Successfully Add Customer Document.</div>
  </div>
  </div>
</div>
@stop