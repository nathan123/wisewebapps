@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>

        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">

             <h2 class="">ADD CUSTOMER FAMILY COMPOSITION</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <div class="section-wrapper clearfix">
            <a href="{!! URL::to('/admin/customers/'.$customers) !!}">Back</a>
              <h3>FAMILY Information</h3>
             
              {!! Form::open(array( 'url' => url('admin/customers/storefamily'), 'role'=>'form', 'id'=>'addfamily' )) !!}

                <div class="form-group">
                   {!! Form::label('number_of_children', 'Number Of Children:') !!}
                   {!! Form::text('number_of_children',null,['class'=>'form-control number']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('status', 'Status :') !!}
                  {!! Form::select('status', [
                     'single' => 'Single',
                     'married' => 'Married',
                     'annulled' => 'Annulled',
                     'widow' => 'Widow',
                     ], null, ['class' => 'form-control'] ) 
                 !!}
                </div>                
                
                <div class="form-group">
                   <input type="hidden" name="customers_id" value="{!! $customers !!}" />
                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                </div>

              {!! Form::close() !!}
            </div>
        </div>
    </div>

<div id="addfamilymodal" class="modal fade" role="dialog" >
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-header clearfix">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  </div>
  <div class="modal-body">Succesfully Added Customers Family.</div>
  </div>
  </div>
</div>
@stop