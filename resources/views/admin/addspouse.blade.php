@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            @include('includes.sidebar')
        </div>

        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">ADD SPOUSE</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <div class="section-wrapper clearfix">
              <a href="{!! URL::to('/admin/customers/'.$customers) !!}">Back</a>
              <h3>Customers Spouse</h3>
             
              {!! Form::open(array( 'url' => url('admin/customers/storespouse'), 'role'=>'form', 'id'=>'addspouse','files'=>true )) !!}

                  <div class="clearfix">
                    <div class="col-md-8">
                      <div class="form-group">
                          {!! Form::label('name', 'Name:') !!}
                          {!! Form::text('name',null,['class'=>'form-control', 'required']) !!}
                      </div>

                      <div class="form-group">
                          {!! Form::label('email', 'Email:') !!}
                          {!! Form::email('email',null,['class'=>'form-control', 'required']) !!}
                      </div>

                      <div class="form-group">
                          {!! Form::label('date_of_birth', 'Date of Birth:') !!}
                          <input type="date" name="date_of_birth" class="form-control" />
                      </div>

                      <div class="form-group">
                          <div class="clearfix">
                            <div class="col-md-4">
                               <label>Type:</label>
                               <select class="form-control" name="primarynumtype">
                                 <option>Select Type</option>
                                 <option value="m">Mobile</option>
                                 <option value="t">Telephone</option>
                               </select>
                            </div>
                            <div class="col-md-7">
                              {!! Form::label('contact_number', 'Contact Number:') !!}
                              {!! Form::text('contact_number',null,['class'=>'form-control']) !!}
                            </div>
                          </div>
                      </div>

                      <div class="form-group">
                        <input type="hidden" name="customers_id" value="{!! $customers !!}" />
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                      </div>
                    </div>
                  </div>                    
                  
              {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop