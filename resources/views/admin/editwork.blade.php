@extends('admin.default')
@section('content')
  <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">EDIT CUSTOMER</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
              <a href="{!! URL::to( 'admin/customers/'.$custwork->customers_id ) !!}">Back</a>
              @if( session('success') )
                <div class="clearfix"><span class="alert alert-success" style="display: inherit;">{!! session('success') !!}</span></div>
               @endif

                  <h2>Edit Customers Work History Information</h2>
                  <div class="customers-info clearfix">
                    <div class="col-md-12">
                      {!! Form::model( $custwork,['method' => 'PATCH','route'=>['admin.customers.work.update',$custwork->id],  'id'=>'editcustomerswork' ]) !!}
                          <div class="form-group">
                            {!! Form::text('designation',null,['class'=>'form-control']) !!}
                          </div>

                          <div class="form-group">
                              <input type="date" name="from" class="form-control" value="{!! $custwork->from !!}" />
                          </div>
                          
                          <div class="form-group">
                              <input type="date" name="to" class="form-control" value="{!! $custwork->to !!}" />
                          </div>  

                          <div class="form-group">
                            {!! Form::text('name_of_company',null,['class'=>'form-control']) !!}
                          </div>

                          <div class="form-group">
                              {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                          </div>
                        
                        {!! Form::close() !!}
                    </div>
                  </div>

                
            </div>
        </div>
    </div>
@stop