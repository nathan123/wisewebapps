@extends('admin.default')
@section('content')
	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
           @include('includes.sidebar')
        </div>
        <div class="col-sm-12 col-sm-offset-3 col-md-10 col-md-offset-2 breadcrumbs">
             <h2 class="">SPOUSE LANGUAGE</h2>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="section-wrapper">
            <a href="{!!  URL::to('admin/customers/'.$customer) !!}">Back</a>
               <h3>Spouse Language Info</h3>
             
                  {!! Form::open(array( 'url' => url('admin/customers/storespouselanguage'), 'role'=>'form', 'id'=>'addspouselanguage' )) !!}
                    <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Language</th>
                            <th>Speaking</th>
                            <th>Listening</th>
                            <th>Reading</th>
                            <th>Writing</th>
                          </tr>
                        </thead>  
                        <tbody>
                            <tr class="add_language">
                              <input type="hidden" name="count" />
                              <td>
                                <div class="form-group">
                                  {!! Form::text('language1',null,['class'=>'form-control required']) !!}
                                </div>
                              </td>
                              <td>
                                <div class="form-group">
                                   {!! Form::text('speaking1',null,['class'=>'form-control number required']) !!}
                                </div>
                              </td>
                              <td>
                                <div class="form-group">
                                    {!! Form::text('listening1',null,['class'=>'form-control number required']) !!}
                                </div>
                              </td>
                              <td>
                                <div class="form-group">
                                  {!! Form::text('reading1',null,['class'=>'form-control number required']) !!}
                                </div>
                              </td>
                              <td>
                                <div class="form-group">
                                  {!! Form::text('writing1',null,['class'=>'form-control number required']) !!}
                                </div>
                              </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="form-group">
                       <span class="addeducationfield" style="cursor: pointer;"><i class="glyphicon glyphicon-plus"></i> Add Language</span>
                    </div>
                    <div class="form-group">
                       <input type="hidden" name="customers_spouse_id" value="{!! $spouse !!}" />
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>

                  {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop