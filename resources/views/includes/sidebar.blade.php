<div class="sidebar-wrapper">
	<ul class="nav nav-sidebar">
		<li><a href="{!! URL::to('/admin/dashboard') !!}"><i class="glyphicon glyphicon-home"></i>&nbsp;  Dashboard<span class="sr-only">(current)</span></a></li>
		<li><a href="{!! URL::to('/admin/customers') !!}"><i class="glyphicon glyphicon-user"></i>&nbsp;  Customers<span class="sr-only">(current)</span>&nbsp;<span class="badge">{!! $count !!}</span></a></li>
		@if( Auth::user()->hasRole('admin') || Auth::user()->hasRole('case_officer') )
			<li><a href="{!! URL::to('/admin/activity') !!}"><i class="glyphicon glyphicon-stats"></i>&nbsp;  Activity Logs<span class="sr-only">(current)</span> &nbsp; <span class="badge"><div id="activity_count"></div></span></a></li>	
		@endif
		@if( Auth::user()->hasRole('admin') )
			<li><a href="{!! URL::to('/admin/users') !!}"><i class="glyphicon glyphicon-home"></i>&nbsp; Users<span class="sr-only">(current)</span></a></li>
			<li><a href="{!! URL::to('/admin/documents') !!}"><i class="glyphicon glyphicon-align-justify"></i>&nbsp; Documents<span class="sr-only">(current)</span></a></li>
		@endif
	</ul>
</div>