<<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		table thead tr th,
		table tbody tr td  {
			text-align: center;
		}
	</style>
</head>
<body>
	<table border="1">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Gender</th>
				<th>Contact No.</th>
				<th>CO</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach( $customer as $key => $value )
				<tr>				
					<td>{!! ucwords( $value->firstname . " " . $value->lastname . " " . $value->middle_name ) !!}</td>
					<td>{!! $value->email !!}</td>
					<td>{!! $value->gender !!}</td>
					<td>{!! $value->contact_number !!}</td>
					<td>{!! $value->user->name  !!}</td>	
					<td>{!! $value->status !!}</td>			
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>